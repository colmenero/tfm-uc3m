# Blockchain Resource Access Control

The goal of this project is to design a distributed system for resource access
control.

It comprises a first layer that acts as a PKI (binds users to their public
keys), and a secondary one that maps each resource to a list of allowed users.
Allowed users are elected from each user WoT via a consensus mechanism. 

The whole logic is implemented as a decentralized application (dApp) consisting of
an *Ethereum* smart contract and a local web page. The web interface makes use of
the *web3.js* library for calling the implemented smart contract (programmed with
the *Solidity* language) from Javascript.

## 0. Whitepaper
Read paper that can be found on the /paper folder of the root directory.

## 1. Installation
Installation only requires to download the project and open the *index.html*
file. This file also imports a css and a javascript file for styling and
programming. Therefore, if the javascript file is bundled properly, the
application will connect to its Ethereum and IPFS nodes and run as expected.

Nevertheless, the default configuration requires to run your own ipfs and
ethereum (geth) clients. To modify the connection parameters, edit the 
*config_ui.json* configuration file.

**Important**: Changing config\_ui.json requires recompiling the bundle.json.

To learn how to modify the javascript code and recompile the final bundle.json,
read the development section.

## 2. Usage
The first thing that an user needs is an account on the geth node that the
app is connecting to (chosen on the config file, which can be edited). Once
an account exists, the user must acquire some ethers for it, by for example 
mining them. Section 3.3.5 delves into more detail of how to create an account.
Assuming the contract is already deployed (section 3.4), let us start the 
web application and create some users associated to the accounts previously
generated.

For each account, an user can be created and a public key can be associated
with it (use the proper one for each account). Next step is to choose one of
the created users, which will be the author, and create a resource. To do so,
estimate its identifier by uploading the file first to the corresponding input
box. After an identifier is returned, create the resource and update the local
WoT table for it (add users to the given identifier). When the local WoT table
has been configured, the input box for estimating the WoT hash should return a
value if the identifier is introduced. Otherwise, an error will be displayed.
Finally, push the local WoT table to IPFS so that the owner can access it. As
it was mentioned previously, the owner must now get notified of the resource
creation through another communication channel. It will then update the global
WoT entry for the created resource with an initial value read from the local
WoT of the author.

At this point, the allowed users should get notified as well, so that they can
update their local WoT tables and push them to IPFS. If the owner updates now
the global table based on the local ones (which could be automated with a
javascript listener), the global table will get built based on the consensus
algorithm. The WoT hahs and the keys for the resource are also updated, so that
users can get the new session key from within the application.

There is already at this stage a shared resource that can be accessed only by
allowed users, which are the ones able to decrypt the session key. A global WoT
table, indicating allowed users, changes depending on local WoT and a consensus
algorithm. Each time the global WoT changes for a resource, its keys are
regenerated.

**Important**: The previous steps can be followed along the videos on the "4.
Examples" section. See also the example videos in the README.files folder.


## 3. Development

### 3.1 IPFS

#### 3.1.1 Installing
Depends on system. E.g ArchLinux: `sudo pacman -S go-ipfs`

Once installed, initialize it: `ipfs init`

From now on, every time we want to run the service, execute the command:
`ipfs daemon`

#### 3.1.2 Configuring CORS
IPFS daemon requires CORS to work with the app. Therefore, edit its
configuration JSON file (~/.ipfs/config) so that it has:
```
"API": {
    "HTTPHeaders": {
      "Access-Control-Allow-Origin": [
        "*"
      ]
    }
  },
```
Note that this may represent a security risk. Consider restricting the access 
to a smaller domain than the wildcard \* (allow from everywhere).

References:

* [IPFS and CORS](https://github.com/INFURA/tutorials/wiki/IPFS-and-CORS)
* [IPFS Issues - chrome station](https://github.com/fbaiodias/ipfs-chrome-station/issues/16)

### 3.2 Compiling the web application
The project is built using [node.js](https://nodejs.org/en/) and several node
libraries are employed, such as the web3.js library for calling the Ethereum 
smart contract, IPFS library for peer to peer stoarage or eth-ecies for
encryption. A full list of them can be found in the *package.json* file. To 
install them, run the common `npm install` command.

However, at the client side, a single web application is provided. The
application should be run as a compiled project that only needs to be open in a
browser to work, without the need to run a local web server on the client.
As a consequence, to be able to run the application without needing node,
developers have to install [browserify](http://browserify.org/), which
generates a compiled javascript bundle that can be called from our application.
[Watchify](https://github.com/browserify/watchify) can also be used to compile
changes automatically while developing, so that there is no need to manually
compile each time a file is modified.
After installing both browserify and watchify, a developer would start its
programming routine by running the following command under the /app/js path:
```
npx watchify src/index.js -o build/bundle.js -d -v --s bundle
```
### 3.3 Geth node

#### 3.3.1 Starting the private network
Before being able to run the geth network, a series of steps have to be
performed. They only need to be done on the first time, as early configuration
steps. Basically, it involves running the following command, which initializes
the network:
```
geth --nodiscover --datadir <yourGethDirectory> init <yourCustomGenesis.json>
```
In order for the command to work, we have to give it a directory for the data.
This directory will be referenced from now on when calling geth. The other 
input of the inizialization command is a Genesis block. This block is read from
a file that must be created previously.

Save a file called yourCustomGenesis.json. E.g.:
```
  {
    "config": {
      "chainId": 66741011,
      "homesteadBlock": 0,
      "eip155Block": 0,
      "eip158Block": 0
    },
    "alloc": {
      "a9ea2dd54b114c39f1dcf8fc3ed2735c71f58683": {"balance": "300000"},
      "63dc6947327483b91ca9f61667d9ff8b6b96c473": {"balance": "400000"}
    },
    "difficulty": "0x0080",
    "gasLimit": "0xffffffff",
    "timestamp": "0x00",
    "coinbase": "0x0000000000000000000000000000000000000000",
    "parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
    "nonce": "0x0000000000000000",
    "mixhash": "0x0000000000000000000000000000000000000000000000000000000000000000"
  }
```
Read references below (particularly from Lee Ting and Brandon Arvanaghi) for
more info on the genesis file.

Summing up, the things required to specify in a private chain were:

  - Custom Genesis File
  - Custom Data Directory
  - Custom NetworkID
  - (Recommended) Disable Node Discovery

References:

* [Ethdocs](http://ethdocs.org/en/latest/network/test-networks.html)
* [Lee Ting Ting](https://medium.com/taipei-ethereum-meetup/beginners-guide-to-ethereum-3-explain-the-genesis-file-and-use-it-to-customize-your-blockchain-552eb6265145)
* [Brandon Arvanaghi](https://arvanaghi.com/blog/explaining-the-genesis-block-in-ethereum/)
* [Parthasarathy Ramanujam](http://iotbl.blogspot.com/2017/03/setting-up-private-ethereum-testnet.html?m=1)
* [Stackexchange](https://ethereum.stackexchange.com/questions/13547/how-to-set-up-a-private-network-and-connect-peers-in-geth)

Extracted from references:

> The things that are required to specify in a private chain are:
>
>  - Custom Genesis File
>  - Custom Data Directory
>  - Custom NetworkID
>  - (Recommended) Disable Node Discovery
> 
> The genesis block is the start of the blockchain - the first block, block 0,
> and the only block that does not point to a predecessor block. The protocol
> ensures that no other node will agree with your version of the blockchain
> unless they have the same genesis block, so you can make as many private
> testnet blockchains as you’d like!
> 
> Explanation of config file:
>
>   - config: General Blockchain configuration.
>       - chainId: The main chain has ID equal to 1. Give it another value to
>         indicate others not to bother with the chain.
>       - homesteadBlock: leave to 0. It indicates that we are using
>         "homestead", i.e. the second major release of Ethereum (first one is
>         called Frontier).
>       - eip155Block: leave to 0. It indicates that we are using "Ethereum
>         Improvement Proposal" 155, accepted to prevent replay attacks.
>       - eip158Block: leave to 0. It indicates that we are using "Ethereum
>         Improvement Proposal" 158, accepted to change how Ethereum clients
>         deal with empty accounts.
>
>   - alloc: First participants of the blockchain and its balances.
>   - difficulty: How hard is to mine a block initially (it increases as the
>     blockchain grows - set by the client, in Geth at the CalcDifficulty
>     function). To establish difficulty write in hexadecimal de probability to
>     successfully mine a block. E.g. 1/1024 means that on average we would need
>     1024 hash operations to mine the block. This would be 1024 -> 0x400.
>   - gasLimit: The maximum number of computations any block on that chain can
>     support. Why? E.g. To avoid spending all our gas in case an error produces
>     a contract that would run indefinitely. We set it high to avoid being
>     limited while testing (as we can create ethers).
>
> We also have other parameters that are meaningless in the genesis block, but
> we added for consistency:
>
>   - timestamp: The output of the Unix time() function when the block was
>     created.
>   - coinbase: Reward from mining block.
>   - parentHash: The hash of the previous block header.
>   - nonce and mixhash: Used to determined if block was mined correctly
>     (verification of proof-of-work). Mixhash is an intermediary value that can
>     be used to perform "lightweight" verification, as verifying the nonce is
>     more computationally expensive.

#### 3.3.2 Firewall iptables

* Allow input/forward and tcp/udp from ports 30303 and 30304.
* Allow input tcp 8545

#### 3.3.3 Running the node
Due to the difference in the connections from Truffle and Web3 that was
mentioned in the previous section, so far what we are doing before running the
migration command is to run geth with:
```
geth --datadir <yourGethDirectory> --networkid 66741011 --nodiscover --rpc --rpcaddr "127.0.0.1" --rpcport "8545" --rpccorsdomain "*" --rpcapi "db,eth,net,web3,personal" console
```
Where in my case, the directory for geth data is under:
```
/home/luis/SharedDrive/Documents/master\_ciberseguridad/TFM/gethData/ethereum-private
```

However, after the contract is deployed and the address in *config_ui.json* is
updated, geth is run with the following command instead:
```
geth --datadir <yourGethDirectory> --networkid 66741011 --nodiscover --ws --wsaddr "127.0.0.1" --wsport "8546" --wsorigins "*" --wsapi "db,eth,net,web3,personal" --preload <path_to_script> console
```
Where `<path_to_script>`  is the path to the following script:
```
var mining_threads = 1
var txBlock = 0

function checkWork() {
if (eth.getBlock("pending").transactions.length > 0) {
    txBlock = eth.getBlock("pending").number
    if (eth.mining) return;
    console.log("  Transactions pending. Mining...");
    miner.start(mining_threads)
    while (eth.getBlock("latest").number < txBlock + 12) {
      if (eth.getBlock("pending").transactions.length > 0) txBlock = eth.getBlock("pending").number;
        }
    console.log("  12 confirmations achieved; mining stopped.");
    miner.stop()
}
else {
    miner.stop()
     }
}

eth.filter("latest", function(err, block) { checkWork(); });
eth.filter("pending", function(err, block) { checkWork(); });

checkWork();
```
The script will be loaded on start and will have the advantage that we will
only mine whenever a new block appears. Otherwise geth will continue mining 
blocks and your machine will spend more power.
**Be very careful!!**: At some point in time certain things stopped working and
trying to debug them took me nowhere. The reason behind was that blocks were
not being mined and I had to change the script to the one that mines more
blocks.

Reference for the script:

* [Stackexchange mine only when pending transactions](https://ethereum.stackexchange.com/questions/3151/how-to-make-miner-to-mine-only-when-there-are-pending-transactions)
* [Stackoverflow stop mining on private net](https://stackoverflow.com/questions/40780358/stop-mining-on-private-net-on-geth-ethrereum)

#### 3.3.4 Connecting several nodes to the private chain
Get your peer info: admin.nodeInfo.enode

Write it to a <yourGethDirectory>/geth/static-nodes.json file with the format:
```
[
  "enode://pubkey@ip:port",
  "enode://pubkey@ip:port",
]
```
The port should be 30303, which is the ethereum node discovery port.
The ip can be found with the *ip addr show* linux command.
One line per node. It should be as explained here:
[Github connecting to geth network](https://github.com/ethereum/go-ethereum/wiki/Connecting-to-the-network#static-nodes)

The other option would be to write in the geth console:
```
admin.addPeer("enode://pubkey@ip:port")
```
But keeps giving me errors.

Verification: admin.peers/net.peerCound

#### 3.3.5 Creating accounts
Accounts can be created from within the geth console using the command:
```
personal.newAccount("<your_password>")
```
And then the account for mining can be set and started (using setEtherbase and
start, as already mentioned in other sections).

However, with this procedure, the account public key will remain hidden to us.
Only the address, which is derived from the last par of the public key, is
visible. In this project the public key will be needed as it will be associated
to an user when creating it.
The whole information can be obtained if the account is first created with
[openssl](https://www.openssl.org/) instead and then imported to geth.
The commands come from [Vincent
Kobel](https://kobl.one/blog/create-full-ethereum-keypair-and-address/) and
require to download a keccak-256 executable from its [github
repository](https://github.com/vkobel/ethereum-generate-wallet):

```
# Generate the private and public keys
openssl ecparam -name secp256k1 -genkey -noout | openssl ec -text -noout > keyFile

# Extract the public key and remove the EC prefix 0x04
cat keyFile | grep pub -A 5 | tail -n +2 | tr -d '\n[:space:]:' | sed 's/^04//' > pubKeyFile

# Extract the private key and remove the leading zero byte
cat keyFile | grep priv -A 3 | tail -n +2 | tr -d '\n[:space:]:' | sed 's/^00//' > privKeyFile

# Generate the hash and take the address part
# Keccack downloaded from repo: https://github.com/vkobel/ethereum-generate-walletj
cat pubKeyFile | ./keccak-256sum -x -l | tr -d ' -' | tail -c 41 > address

# Import the private key to geth
geth account import privKeyFile
```
If your private network is running with your own --datadir, search for the
generated file under the default keystore directory (usually
~/.ethereum/keystore) and move it there. Check running the node as usual and
doing eth.accounts to list it.

### 3.4 Modifying the smart contract
Solidity files are under the contracts/ folder. However, the web application
calls the smart contract deployed on the ethereum network (or your private
network). Therefore, in order to make changes to the smart contract, it is
necessary not only to edit the Solidity scripts, but also to compile and 
deploy them again. Once it is done, the configuration file of the web
application (*config_ui.json*) must be updated with the new address and ABI
(found in build/contracts/<contract_name>.json, PKI.json in our case).

To compile the contract, [truffle](https://www.trufflesuite.com/) is needed.
Then, the following command has to be issued:
```
npx truffle migrate --reset
```

Of course, this only works if the network where is going to be deployed can
be read. Truffle configuration file is the one named *truffle-config.js*.
This project has already configured some parameters such as the Solidity
version used to compile it or the network where the project is going to be
compiled. By default the project works with a test network running on localhost
port 8545 and with an identifier 66741011.

The information from the Ethereum network must be reflected on two files: the
truffle configuration file (*truffle-config.js*) we just talked about and the
application configuration file (*config_ui.json*). There is however a small
difference in the default behaviour. By default in the web UI the project works
with websockets over the localhost port 8546 (config\_ui.json -> providers ->
web3). The reason is to make use of the advantages of websockets when using
web3.js. On the other hand, Truffle is configured by default to connect via RPC
to port 8545.

This is just a matter of configuring it properly to the right provider.
Developers do not even have to run their own ethereum nodes if they use
alternatives such as Infura. Nevertheless, next section deals with the process
of running a node, in case we want to set it up locally (what current developer 
does).

**Important**: To deploy the contract correctly, it is not enough to have an
ethereum node running. It must also be mining blocks and the account has to be
unlock (authentication needed).
After getting a geth console, input the commands:
```
personal.unlockAccount(<your_account>, '<your_password>')
miner.start()
```
The account for mining can be set with `miner.setEtherbase(<your_account>)`.
To get a list of accounts run `eth.accounts` within the geth console.

#### 3.4.1 Remix IDE 
To modify the smart contracts, the [Remix
IDE](https://remix.ethereum.org/#optimize=false) (Integrated Development
Environment) is an useful tool that can be employed. It allows not only
to edit the files with things such as syntax highlightings, but it also 
includes its own testing network where the smart contract can be deployed and 
run. It is a nice interface for prototyping and getting a first preview of
how the programs behaves. After a successful execution on Remix, the contract
can be deployed on the geth test network for a more exact simulation.

### 3.5 Alternative: Using infura 
An alternative to running your own geth node is to use a third party
provider such as infura. Infura provides the user with a browser extension
called *metamask*, which acts as a wallet. Infura itself runs as a miner, so
that the contract can be deployed to the network and later accessed through the
web browser extension.

Though this project has been tested throughly only with a running local geth
node, it can also run with the metamask extension. To do so:

- Deploy the project through infura using truffle. A network named
  *infura_rinkeby* is already given in the truffle configuration file, it just
  needs to be adapted to the user's infura acccount. First make an account on
  Infura and create a project. Then, modify the endpoint so that it points to
  your project ID and configure an environment variable containing your
  ethereum account mnemonic. Your account mnemonic is a 12 word string that
  acts as your account private key. It can be generated with an [online
  tool](https://iancoleman.io/bip39/). After generating the account, import it
  to the metamask extension and get some ether through an [online
  faucet](https://testnet.help/es/ethfaucet/rinkeby) Finally, deploy the
  contract.

```
<edit truffle-config.js -> variable infura_endpoint>
export mnemonic=<string>
npx truffle migrate --network infura_rinkeby
```

- The user just needs to open the page with a browser that has metamasked
  configured. The rest of the procedure is the same, with the exception that
  authentication is handled by Metamaks (but click *authenticate* even without
  entering password, so that the appropiate tabs get activated).

**Important**: Metamask does not work with local files, so the application
has to be served when using it. Running a simple python server on the
application folder solves it:
```
python -m http.server
```

**To solve**: How to get public key if account generated through mnemonic?

References:

 [Trufflesuite.com](https://www.trufflesuite.com/tutorials/using-infura-custom-provider)

### 3.6 Using the session key 
As a result from using the dApp a user receives the session key and
initialization vector for a particular resource (as long as he is authorized).
The session Key is a 256bit AES key, though it can be easily changed.

To encrypt the resource the author can use openssl:
```
openssl enc -aes256 -in <resource> -out <resource_encrypted> -K <key> -iv <iv>
```

And to decrypt it the user can execute:
```
openssl enc -aes256 -in <resource_encrypted> -out <resource> -K <key> -iv <iv> -d
```

Another alternative is to use directly the encrypting/decrypting feature
provided by the web application. As an author, if we provide the app with a
file, a session key and a IV, the encrypted version will be generated and
pushed to IPFS (updating a mapping in Ethereum so that it can later be
retrieved). As an user we can provide the same values in order to get the
decrypted version out of the encrypted version that can be found on IPFS.

Note: Right now only the author can create/update content. It would be cool to
have all allowed users to be able to push it into a new IPFS location.
However, for privacy reasons we are not keeping track of the global WoT in
Ethereum so it cannot be checked. Another option would be to do it through the
owner.

See example 4.8.

## 4. Examples

### 4.1 Authentication

![Authentication](README.files/authentication.gif)

### 4.2 Create User

![Create user](README.files/createUser.gif)

### 4.3 Create Resource

![Create Resource](README.files/createResource.gif)

### 4.4 Add users to local resource Web Of Trust

Let's start by adding the initial WoT list as the author of the resource.  In
this example we are adding, aside from the own author, four more users that
are allowed to edit the resource.

![Update local WoT](README.files/updateLocalWot.gif)

Repeat the same process for the rest of the users (two to five), but with a
modification: do not include user5 (except for user5 who would like to
include itself). User5 will be our test user, if the update process goes
well, he will not be able to access the resource, as the consensus algorithm
establishes that it is not trusted by the majority of users.

### 4.5 Update Global WoT

The owner can now update the global WoT and push it to IPFS. The resource 
WoT hash and keys can be updated in Ethereum as well. This is done in the 
next gif video.

![Update global WoT](README.files/updateGlobalWot.gif)

Note that the global WoT must be first updated locally with, at least, the
value of the author. In the previous video it was updated with only that
value so clicking once the update button returns the global WoT as seen by
the author (the only node on the local list at the time). Clicking once more
the update button, runs the consensus algorithm on the new users. As a
consequence, user5, which as not accepted by the majority, gets removed from
the list.

### 4.6 Checking WoT hash and Session Key for the resource

After the updating process carried out by the owner, each user can check the 
resulting data. Next video shows:

- How the hash of the allowed users for that resource (WoT hash) matches.
  That means that the hash computed by the owner using the global WoT can be
  read by the user from Ethereum. Users can therefore check if the global 
  WoT matches the one from their local WoT.

- How the sesssion key can be decrypted and obtained by an allowed user. This 
  session key would then be used to the decrypt the shared file.

![Checking output](README.files/checkingOutput.gif)

### 4.7 Opentimestamp

Using the node Opentimestamp module, resources can be timestamped as well.
The following video shows the procedure.

![OTS](README.files/ots.gif)

Note that timestamping takes a while. If a *pending attestation* message is 
displayed, just like happens in the video, wait for a while longer.

The library works by connecting to a set of default nodes that receive requests
and mine transactions on the blockchain. Therefore, it requires an Internet
connection (to connect to the miners).


### 4.8 Upload/Download resource
Once we have a session key the file can be encrypted and uploaded to IPFS
(if author) or read from IPFS and decrypted (if user).

This part can be thought to be independent of the project's goal. Once nodes
have a way of sharing a session key for a specific resource with only certain
users, sharing of the encrypted file is not really relevant. However, it is
included so that we can see that sharing of files can be done over the same
IPFS peer storage network and that Ethereum can be used to map resources to
the location of its encrypted contents.

The next gif image is an example of uploading and downloading a resource using
a given IV and session key (that could have been obtained using the webapp and
previous procedures). We can check how final decrypted content is the same as
originally.

So far the author is the only one who can upload/upgrade a resource. There is a
system for tracking changes so that allowed users can download the newer
version of a resource or older ones.

TODO: Resource keys are only saved for the last version. Therefore, so far
users must know the old ones to decrypt data. This can be easily solved by
implementing the same system for keys than there is for the data itself (CID
pointing to registry instead of directly).

![OTS](README.files/upDownResource.gif)

### 4.9 Additional examples
More examples can be found on the README.files folder. The following videos are
available:

- **example\_main.mp4**: Example running on the main Ethereum network through metamask
  and infura. User creation, resource creation, WoT configuration, resource 
  upload/download...

- **example0.mp4**: Example that goes from creating users to getting a session key
  associated with a resource. Video made by composing the recording of two
  screens: one from a laptop and one from a computer. Each one runs their own
  miners but they are connected to the same network so data can be exchanged.

- **example1.mp4**: Assuming already configured users, a resource is created and
  shared with another node. The author uploads the resource and it is then
  downloaded by the allowed user. We check that non allowed users are not able
  to obtain the session key needed to decrypt the resource.

## 5. Chat server

To ease actions, a communication channel is needed. For example, if a resource
is created, the owner must be informed to include it in the global WoT with an 
initial value. To ease this communication a chat server is provided (run 
using node chat/server.js). However, this goes against the p2p philosphy that
we had in mind so future work involves to replace the server with a fully
distributed communication protocol such as bitmessage.
