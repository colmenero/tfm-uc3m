//  Mnemonic just defined to avoid error if undefined.
//  This mnemonic is not a real one. Set yours as an environement variable
const HDWalletProvider = require("truffle-hdwallet-provider");
const mnemonic = process.env.mnemonic
  || 'bench tilt song garden lounge faith paper evil december radar copper mean';

module.exports = {
  networks: {
    development: {
     host: "127.0.0.1",     // Localhost (default: none)
     port: 8545,            // Standard Ethereum port (default: none)
     network_id: "66741011"       // Any network (default: none)
    },
    infura_rinkeby: {
      provider: new HDWalletProvider(
        mnemonic,
        'https://rinkeby.infura.io/v3/d917703aeae149819efdc714df0b8696'
       ),
      network_id: '*'
    },
    infura_ropsten: {
      provider: new HDWalletProvider(
        mnemonic, 'https://ropsten.infura.io/v3/d917703aeae149819efdc714df0b8696'
       ),
      network_id: '*'
    },
    infura_main: {
      provider: new HDWalletProvider(
        mnemonic, 'https://mainnet.infura.io/v3/d917703aeae149819efdc714df0b8696'
       ),
      network_id: '*',
      gas:3500000,
      gasPrice: 20000000000
    }
  },

  // Set default mocha options here, use special reporters etc.
  mocha: {
    // timeout: 100000
  },

  // Configure your compilers
  compilers: {
    solc: {
      version: "0.5.1",    // Fetch exact version from solc-bin (default: truffle's version)
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      // settings: {          // See the solidity docs for advice about optimization and evmVersion
      //  optimizer: {
      //    enabled: false,
      //    runs: 200
      //  },
      //  evmVersion: "byzantium"
      // }
    }
  }
}
