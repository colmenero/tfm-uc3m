const io = require('socket.io')(3000)
//io.set('origins', '*:*'); // not need to configure cors


const CONFIG = require('../config_ui.json');

const iosp_gwot = io.of(CONFIG.providers.io.namespace_globalWot)

iosp_gwot.on('connection', function(socket) {
  console.log('User connected');
  socket.emit(
    'welcome', 'Welcome! You are connected to the common chat'
  );

  socket.on('broadcast', function(msg) {
    socket.broadcast.emit('author', msg);
  });

  socket.on('resource', function(msg) {
    socket.broadcast.emit('resource', msg);
  });

  socket.on('localWot', function(msg) {
    socket.broadcast.emit('localWot', msg);
  });

});

/* -------------------------------------------------------------------------
 *                          NOTES
 * -------------------------------------------------------------------------
 *
 * Send to everyone including sender
 *   io.of(CONFIG.providers.io.namespace_globalWot).emit('msg', 'message');
 * Send to everyone but sender
 *   socket.broadcast.emit('msg', 'message');
 * It is supposed to not work in socketio 2.x. May need to upadate it in the
 * future to:
 *   io.sockets.sockets[data.senserID].broadcast.emit('msg', 'message')
 * Where data is the message from the socket.
 */
