/*
 * **************************************************************************
 *                             IMPORTS
 * **************************************************************************
 * OpenTimestamps module seems a mess, generates errors in node and warnings
 * in the js code
 */
const Web3 = require('web3');
const ipfsClient = require('ipfs-http-client');
const bs58 = require('bs58');
const crypto_node = require('crypto');
const ecies = require('eth-ecies');
const OpenTimestamps = require('javascript-opentimestamps');
const io = require('socket.io-client');

/*
 * **************************************************************************
 *                             READ CONFIG FILE
 *
 * Config file is going to be read only once, here in common.js
 * The rest of scripts will use the variables set here.
 * **************************************************************************
 */
const CONFIG = require('../../../config_ui.json');

/*
 * **************************************************************************
 *                             CONFIGURE LIBRARIES
 * **************************************************************************
 */

/*
 *                             IPFS & WEB3
 * --------------------------------------------------------------------------
 */
const [web3, ipfs] = load_metamask();

// Log error if could not connect to web3 provider
web3.eth.net.isListening()
  .then(() => {
    console.log('*****************************************************');
    console.log("Web3 successfully connected to provider"); // print provider?
    console.log('*****************************************************');
  })
  .catch(error => {
    console.log(error);
    console.log('*****************************************************');
    console.log('Could not connect to web3 ws provider');
    console.log('Check that the ethereum client (e.g. geth) is running');
    console.log('*****************************************************');
  });

/*
 *                       ETHEREUM SMART CONTRACT
 * --------------------------------------------------------------------------
 */
const CONTRACT_ADDRESS = CONFIG.address;
const CONTRACT_ABI = CONFIG.outputABI['abi'];
const myContract = new web3.eth.Contract(CONTRACT_ABI, CONTRACT_ADDRESS);

/*
 *                              SOCKETIO
 * --------------------------------------------------------------------------
 */

const socket_gwot = io.connect(
  CONFIG.providers.io.server + CONFIG.providers.io.namespace_globalWot,
  {reconnect: true}
);
// If reconnection not set to false, the node will keep polling for the
// chat server. As it is optional, let us better ignore the server if not up.
socket_gwot.io.on('connect_error', function(err) {
  console.log(
    'Error connecting to chat server. Stop trying to connect. ' +
    'Refresh if necessary'
  );
  socket_gwot.io.reconnection(false);
});

/*
 * **************************************************************************
 *                       GENERAL FUNCTIONS AND VARIABLES
 * **************************************************************************
 */
/*
 *                             CONSTANTS
 * --------------------------------------------------------------------------
 */
const EMPTYADDR = "0x0000000000000000000000000000000000000000";
const EMPTYB32 = "0x0000000000000000000000000000000000000000000000000000000000000000";
const EMPTYPUBKEY = EMPTYB32 + EMPTYB32.slice(2);
const OWNERNAME = CONFIG.vars.ownerName;
const OWNERADDR = new Promise(function(resolve, reject) { // TODO: handle error
  myContract.methods.queryAddress(web3.utils.asciiToHex(OWNERNAME))
  .call()
  .then(ownerAddress => {resolve(ownerAddress)})
});

/*
 *                             VARIABLES
 * --------------------------------------------------------------------------
 *
 * TODO: save localWot and globalWot as localStorage?
 *
 * ==========================================================================
 * VERY IMPORTANT!!!!
 *
 * Here we have just an inizialization.
 * After that they can only be modified in one file:
 *    - global wot is modified in admin.js
 *    - localwot is modified in local.js 
 *    - queriedWot is local to common.js only
 *  ==========================================================================
 */
var localWot = {};
var globalWot = {};
var queriedWot = {};

/*
 *                             FUNCTIONS
 * --------------------------------------------------------------------------
 */

function load_metamask() {
  // Modern dapp browsers...
  var web3, ipfs;
  if (window.ethereum) {
    console.log('Connecting to metamask...');
    web3 = new Web3(ethereum);
    try {
      // Silent a warning that appears otherwise
      ethereum.autoRefreshOnNetworkChange = false;
      // Request account access if needed
      ethereum.enable();
      // IPFS through infura
      ipfs = ipfsClient({
        host: 'ipfs.infura.io',
        port: 5001,
        protocol: 'https'
      });
    } catch (error) {
      // User denied account access...
    }
  }
  // Legacy dapp browsers...
  else if (window.web3) {
    console.log('Connecting to legacy metamask...');
    web3 = new Web3(web3.currentProvider);
    // IPFS through infura
    ipfs = ipfsClient({
      host: 'ipfs.infura.io',
      port: 5001,
      protocol: 'https'
    });
  }
  // Non-dapp browsers...
  else {
    console.log('No metamask detected. Connecting to local provider...');
    const WEB3_PROVIDER = CONFIG.providers.web3;
    web3 = new Web3(
      Web3.givenProvider ||
      new Web3.providers.WebsocketProvider(WEB3_PROVIDER),
      null, {}
    );
    const IPFS_PROVIDER = CONFIG.providers.ipfs;
    ipfs = ipfsClient(IPFS_PROVIDER); // TODO: Check connection works
  }
  return [web3, ipfs];
}

function deepCopy_js(input) {
  /*
   * output = input; // shallow copy
   */
  return JSON.parse(JSON.stringify(input));
}

function checkOwner_js(userAddress) {
  return OWNERADDR.then(ownerAddress => {
    return (userAddress.toLowerCase() == ownerAddress.toLowerCase());
  });
}

function computeHash_js(users) {
  /*
   * Hash used for comparing resources wot
   *
   * Input: array of users
   */
  function hash_alg(input) {
    /*
     * Any hash could be chosen as long as it maches programmed ethereum data
     * type for the wot property of a resource (bytes32).
     */
    return web3.utils.soliditySha3(input);
  }

  users.sort()
  hashes = users.map(elem => hash_alg(elem));
  while (hashes.length>1) {
    let nhashes = hashes.length;
    let nitera = Math.floor(nhashes/2);

    let hashes_new = [];
    for (let i=0; i<nitera; i++) {
      let new_elem = hashes[2*i].concat(hashes[2*i+1]);
      hashes_new.push(hash_alg(new_elem))
    }

    let nhashes_isodd = (nhashes % 2);
    if (nhashes_isodd) {
      let last_elem = hashes[nhashes-1];
      hashes_new.push(last_elem);
    }
    hashes = deepCopy_js(hashes_new);
  }

  return hashes[0];
}

function getQueriedWot_js(flag) {
  return queriedWot;
}

function multihash_bytes2Hex_js(value_bytes) {
  /*
   * TODO: Rename to multihash_sol2bs58_js
   * Return wot value in bs58 format or null if not found/error
   */
  if ((value_bytes == null) || (value_bytes == EMPTYB32)) {
    return output = null;
  } else {
    const value_header = '1220' + value_bytes.slice(2); // returns with 0x
    // Buffer.from() does not accept 0x format but directly data
    const value_hex = Buffer.from(value_header, 'hex');
    output = bs58.encode(value_hex);
    return output;
  }
}

function multihash_hex2Bytes_js(value_bs58) {
  /*
   * TODO: Rename to multihash_bs58toSol_js
   * Note: there is still pieces of code in the project not using this function
   * Update gradually
   * E.g. setWid_ethereum
   */
  let value_buff = bs58.decode(value_bs58);
  let value_hex = value_buff.toString('hex');
  let value_noHeader = '0x' + value_hex.slice(4);
  return value_noHeader;
}

async function queryWid_inner_ethereum(userName) {
  /*
   * Return wot value in bs58 format or null if not found/error
   */
  const userName_bytes = web3.utils.asciiToHex(userName, 32);
  let output = '';
  return myContract.methods.queryUserWot(userName_bytes).call()
  .then(wot_bytes => {
    wot_value = multihash_bytes2Hex_js(wot_bytes);
    return wot_value;
  });
}

async function sign(string, address) {
  /*
   * Made into a separated function as the signing algorithm could change.
   *
   * web3.eth.sign is deprecated and it is advisable to use
   * web3.eth.personal.sign
   *
   * web3.eth.personal.sign accepts a password as a third argument. This
   * argument can be left blank ('') if metamask is being used.
   */
  const hash = web3.utils.soliditySha3(string);
  let signature;
  if (window.web3.currentProvider.isMetaMask === true) {
    signature = web3.eth.personal.sign(hash, address, '')
      .then(output => {return output});
  } else {
    signature = web3.eth.sign(hash, address)
      .then(output => {return output});
  }
  return signature;
}

async function verify_signature(userName, string, signature) {
  /*
   * TODO: unify naming format of all functions, what a mess!!
   */
  const hash = web3.utils.soliditySha3(string);
  return web3.eth.personal.ecRecover(hash, signature).then(
    signerAddress => {
      const userName_bytes = web3.utils.asciiToHex(userName, 32);
      return myContract.methods.queryAddress(userName_bytes).call()
        .then(userAddress => {
          return (signerAddress.toLowerCase() == userAddress.toLowerCase()) ?
            true : false;
        });
    }
  );
}
async function verifySignature_fromAddress(userAddress, string, signature) {
  let hash = web3.utils.soliditySha3(string);
  return web3.eth.personal.ecRecover(hash, signature).then(signerAddress => {
    return (signerAddress.toLowerCase() == userAddress.toLowerCase()) ?
      true : false;
  });
}

function queryWot_ipfs_inner(fileHash, userName, current_pre){
  /*
   *  Errors:
   *        If the provided file identifier does not exist on the ipfs network,
   *        there is no error returned. The only way no know that something
   *        went wrong is timeouts.
   *
   *        Another error could be that the file exists but it is not a wot.
   *        This could be solved by using a try catch inside the files.forEach
   *        but not worth it at this point in time
   */
  ipfs.get(fileHash, function (err, files) {
    if(!err) {
      files.forEach((file) => {
        const contents = file.content.toString('utf8');
        const lines = contents.split('\n');
        const signature = lines[0];
        const wot = lines[1];
        current_pre.innerHTML = wot;
        queriedWot = JSON.parse(wot);
        verify_signature(userName, wot, signature)
          .then(success => {
            current_pre.innerHTML += "\nSignature verification: " + success;
          });
      });
    } else {current_pre.innerHTML += 'Error occured\n' + err;}
  })
}

function setWid_ethereum(userName, fileHash_bs58, flag_public) {
  const userName_bytes = web3.utils.asciiToHex(userName, 32);
  const fileHash_sol = multihash_hex2Bytes_js(fileHash_bs58);

  myContract.methods.setUserWot(userName_bytes, fileHash_sol, flag_public)
    .send({from: web3.eth.defaultAccount})
}

function pushWot_ipfs_inner(userAddress, current_pre, wot, flag_est, flag_pub)
{
  myContract.methods.queryUser(userAddress).call()
    .then(userName_bytes => {
      const userName = web3.utils.hexToUtf8(userName_bytes);

      if (userName_bytes == EMPTYB32) {
        current_pre.innerHTML = "Your current Ethereum account doesn't";
        current_pre.innerHTML += "have an associated user yet";
        return false;
      }

      wot_string = JSON.stringify(wot);
      sign(wot_string, userAddress).then(signature => {
        const data = signature + '\n' + wot_string;
        let buff = Buffer.from(data);


        // Push to IPFS
        ipfs.add(buff, {'onlyHash': flag_est}, function (err, file) {
          let success = false;
          let output = "";
          if (err) {
            console.log(err);
          } else {
            let fileHash_bs58 = file[0].hash;
            if (!flag_est) {
              setWid_ethereum(userName, fileHash_bs58, flag_pub);
              console.log('User: ' + userName + '; CID:' + fileHash_bs58);
            }
            success = true;
            output +=  "Hash: " + fileHash_bs58;
          }
          output += '\n<p>Status: ' +  success + '<p>';
          current_pre.innerHTML = output;
        });
      });
    });
};

async function queryPubKey_ethereum(userName_bytes) {
  return myContract.methods.queryPubKey_half(userName_bytes, false).call()
    .then(half0_bytes => {
      return myContract.methods.queryPubKey_half(userName_bytes, true).call()
        .then(half1_bytes => {
          let pubKey = half0_bytes + half1_bytes.slice(2);
          return pubKey;
        });
    });
}

function encryptAsym_js(publicKey, data_plain) {
  /*
   * publicKey: without the 0x at the beginning
   *
   * Return: hex string
   */
  let publicKey_buff = new Buffer(publicKey, 'hex');
  let data_plain_buff = new Buffer(data_plain, 'hex');
  let data_enc_buff = ecies.encrypt(publicKey_buff, data_plain_buff);
  let data_enc_hex = data_enc_buff.toString('hex');
  return data_enc_hex;
}

function decryptAsym_js(privateKey, data_encrypted) {
  /*
   * privateKey: without the 0x at the beginning
   *
   * Return hex string
   */
  let privateKey_buff = new Buffer(privateKey, 'hex');
  let data_enc_buff = new Buffer(data_encrypted, 'hex');
  let data_dec_buff = ecies.decrypt(privateKey_buff, data_enc_buff);
  let data_dec_hex = data_dec_buff.toString('hex');
  return data_dec_hex;
}

function encryptSym_js(sessionKey, iv, data) {
  /*
   * Input:
   *    sessionKey: hex string
   *    iv: hex string
   *    data: buffer
   * Return: buffer
   */
  let encrypted = false;

  // Define cipher properties
  const algorithm = 'aes-256-cbc';
  let cipher = crypto_node.createCipheriv(
    algorithm,
    Buffer.from(sessionKey, 'hex'),
    Buffer.from(iv, 'hex')
  );

  encrypted = cipher.update(data);
  encrypted = Buffer.concat([encrypted, cipher.final()]);

  return encrypted;
}

function decryptSym_js(sessionKey, iv, encrypted) {
  /*
   * Input:
   *    sessionKey: hex string
   *    iv: hex string
   *    encrypted: buffer. Coud be string but would need to specify encoding.
   * Return: buffer
   *
   */

  // Set decipher
  const algorithm = 'aes-256-cbc';
  let decipher = crypto_node.createDecipheriv(
    algorithm,
    Buffer.from(sessionKey, 'hex'),
    Buffer.from(iv, 'hex')
  );

  let decrypted = decipher.update(encrypted);
  decrypted = Buffer.concat([decrypted, decipher.final()]);

  return decrypted;
}

function getRandomHex(length_bytes) {
  /*
   * Return: hex string
   *
   * @dev:
   *  A single byte generates two hex digits
   *  No 0x at the beginning
   *
   * @reference:
   *  https://blog.abelotech.com/posts/generate-random-values-nodejs-javascript/
   *
   */
  let length_hex = length_bytes*2;
  return crypto_node
    .randomBytes(Math.ceil(length_bytes))
    .toString('hex')
    .slice(0, length_hex);
}

function generateSessionKey_js() {
  /*
  * 32 Bytes (256bit) session key
  * Randomly generated
  * To be used with AES
  *
  * @use:
  *  The author should encode the file using the session key and make it
  *  available, e.g. by publishing it to IPFS
  *    openssl enc -aes256 -in <file> -out <file_encrypted> -base64 -K <key> -iv <iv>
  *
  *  Users should then decode the file when they get the session key:
  *    openssl enc -aes256 -in <file_encrypted> -out <file> -base64 -K <key> -iv <iv> -d
  *
  *  Reference:
  *    https://wiki.openssl.org/index.php/Enc
  *    https://devops.datenkollektiv.de/using-aes-with-openssl-to-encrypt-files.html
  *
  * @notes:
  *  Saved only (ciphered) in IPFS so length could be easily increased if
  *  needed (on the other hand 64B public keys would be more troublesome).
  *
  */
  return getRandomHex(32);
}

function generateIV_js() {
  /*
    * AES256 -> 128bit IV
    */
  return getRandomHex(16);
}

function keys_hex2bs64_js(input) {
  let tmp = new Buffer(input, 'hex');
  return tmp.toString('base64');
}

function keys_bs64toHex_js(input) {
  let tmp = new Buffer(input, 'base64');
  return tmp.toString('hex');
}

function updateKeysID_ethereum_ipfs(resourceHash, allowedUsers) {
  /*
    * allowedUsers = globalWot[resourceHash]
    */

  const sessionKey = generateSessionKey_js();
  const iv = generateIV_js();
  let keyFile = {'iv': keys_hex2bs64_js(iv)};

  return new Promise(function (resolve, reject) {

    // Get all public keys
    let promises_pubKeys = [];
    allowedUsers.forEach(userName => {
      const userName_bytes = web3.utils.asciiToHex(userName, 32);
      promises_pubKeys.push(
        queryPubKey_ethereum(userName_bytes)
      )
    });

    // When all public keys are read, write ciphered session keys to object
    // which will be later stringified and saved as JSON string
    Promise.all(promises_pubKeys).then(promises => {
      promises.forEach((pubKey, idx) => {
        if (pubKey  !== EMPTYPUBKEY) {
          const pubKey_noprefix = pubKey.slice(2);
          const sessionKey_enc = encryptAsym_js(pubKey_noprefix, sessionKey);
          keyFile[allowedUsers[idx]] = keys_hex2bs64_js(sessionKey_enc);
        }
      });

      // Push content to ipfs file and set new key ID in resource
      ipfs.add(Buffer.from(JSON.stringify(keyFile)), function (err, file) {
        if (err) {
          console.log(err);
          reject({'success': false, 'value': 'ErrIPFSadd'});
        }

        const keyFileCID_bs58 = file[0].hash;
        const keyFileCID = multihash_hex2Bytes_js(keyFileCID_bs58);
        myContract.methods.modifyResourceKeys(resourceHash, keyFileCID)
          .send({from: web3.eth.defaultAccount})

        resolve({
          'success': true,
          'value': {
            'resourceHash': resourceHash,
            'CID': keyFileCID_bs58,
            'iv': iv,
            'sk': sessionKey
          }
        });
      });
    });
  });
}

function modifyResourceWotHash_ethereum(resourceHash, allowedUsers) {
  const newWotHash = computeHash_js(allowedUsers);
  return new Promise(function (resolve, reject) {
    myContract.methods.modifyResourceWot(resourceHash, newWotHash)
    .send({from: web3.eth.defaultAccount})
    .on('transactionHash', hash => {
      resolve({
        'transactionHash': hash,
        'wotHash': newWotHash
      })
    });
  });
}

function readEncryptedResource_ipfs(CID, sessionKey, iv) {
  return new Promise(function (resolve, reject) {
    ipfs.get(CID, function (err, files) {
      if (!err) {
        files.forEach((file) => {
          const encrypted = file.content;
          const decrypted = decryptSym_js(sessionKey, iv, encrypted);
          resolve({
            'status': true,
            'value': decrypted
          });
        });
      } else {
        reject({
          'status': false,
          'value': 'NoResourceIPFS'
        });
      }
    });
  });
}

function readRegistry_ipfs(resourceHash) {
  return new Promise(function (resolve, reject) {
    myContract.methods.queryResourceFile(resourceHash).call()
    .then(fileID_bytes => {
      if (fileID_bytes === EMPTYB32) {
        reject({
          'status': false,
          'CID': false,
          'value': 'NoRegSol'
        });
      } else {
        fileID = multihash_bytes2Hex_js(fileID_bytes);
        ipfs.get(fileID, function (err, files) {
          if(!err) {
            files.forEach((file) => {
              const registry = file.content.toString('utf8');
              resolve({
                'status': true,
                'CID': fileID,
                'value': registry
              });
            });
          } else {
            reject({
              'status': false,
              'CID': false,
              'value': 'NoRegIPFS'
            });
          }
        })
      }
    });
  });
}

function createRegistry_ipfs(resourceHash, registry) {
  /*
   * Input: buffer
   */
  return new Promise(function (resolve, reject) {
    ipfs.add(registry, function (err, file) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        const registryCID_bs58 = file[0].hash;

        // Set registry CID on ethereum
        const registryCID_hex = multihash_hex2Bytes_js(registryCID_bs58);
        myContract.methods.modifyResourceFile(resourceHash, registryCID_hex)
        .send({from: web3.eth.defaultAccount})
        resolve(registryCID_bs58);
      }
    });
  });
}

function readInputFile_js(file) {
  return new Promise((resolve, reject) => {
    if (file) {
      var reader = new FileReader();
      reader.readAsArrayBuffer(file);
      reader.onload = function (evt) {
        contents = Buffer.from(evt.target.result);
        resolve(contents);
      }
    } else {
      reader.onerror = function (evt) {
        contents = false;
        reject(contents);
      }
    }
  });
}

function downloadResource_ipfs(resourceHash, version, sessionKey, iv) {
  return new Promise((resolve, reject) => {
    readRegistry_ipfs(resourceHash)
    .then(output_registry => {
      const registry = output_registry['value'];

      // read CID
      const lines = registry.split('\n');
      const versionIndex = (version > (lines.length-1)) ? 0 : version;
      const resourceCID = lines[(lines.length - 1) - versionIndex];

      readEncryptedResource_ipfs(resourceCID, sessionKey, iv)
      .then(output_decrypted => {
        const decrypted = output_decrypted['value'];
        resolve({
          'status': true,
          'value': {
            'registry': registry,
            'content': decrypted
          }
        })
      })
      .catch(output_decrypted => reject(output_decrypted));
    })
    .catch(output_registry => reject(output_registry));
  });
}


function checkAuthor_ethereum(resourceHash) {
  /*
    * Author must exist and user==author
    */
  return new Promise(function(resolve, reject) {
    const userAddress = web3.eth.defaultAccount;
    myContract.methods.queryResourceAuthor(resourceHash).call()
    .then(author_bytes => {
      myContract.methods.queryUser(userAddress).call()
      .then(userName_bytes => {
        if (
          (author_bytes != EMPTYB32) &&
          (author_bytes === userName_bytes)
        ) {resolve(true)}
        else {reject(false)}
      });
    });
  });
}

function uploadResource_ipfs(resourceHash, contents) {
  /*
   * TODO: Check resource exists
   *       Right now queryResourceAuthor filters
   */
  return new Promise((resolve, reject) => {
    checkAuthor_ethereum(resourceHash)
    .then(output_check => {if (output_check) {
      ipfs.add(contents, function (err, file) {
        if (err) {
          console.log(err);
        } else {
          const resourceCID = file[0].hash;
          readRegistry_ipfs(resourceHash)
          .then(output => {
            const registry = Buffer.from(
              output['value'] + '\n' + resourceCID
            );
            createRegistry_ipfs(resourceHash, registry).then(newCID => {
              resolve({
                'status': true,
                'value': newCID,
                'content': registry
              });
            });
          })
          .catch(output => {if (output['value'] === 'NoRegSol') {
              const registry = Buffer.from(resourceCID);
              createRegistry_ipfs(resourceHash, registry).then(newCID => {
                resolve({
                  'status': true,
                  'value': newCID,
                  'content': registry
                });
              });
          }});
        }
      });
    }})
    .catch(output_check => {
      reject({
        'status': false,
        'value': 'ErrAuthor'
      });
    });
  });
}

function uploadEncryptedResource_ipfs(resourceHash, contents, sessionKey, iv) {
  /*
   * Contents is the result of reading file with readInputFile (TODO: check
   * type)
   *
   * Encrypt contents and upload them to IPFS so that changes can be tracked.
   * The CID from Ethereum does not point directly to the encrypted
   * resource (contents). Instead, it is written the CID of a text file
   * acting as a registry of versions.
   * This registry has to be created/modified and uploaded to IPFS  as well.
   *
   */
  return new Promise((resolve, reject) => {
    const encrypted = encryptSym_js(sessionKey, iv, contents);
    if (encrypted === false)
      reject({
        'status': false,
        'value': 'ErrEnc'
      });
    else
      uploadResource_ipfs(resourceHash, encrypted)
      .then(output => {
        output['encrypted'] = encrypted;
        resolve(output);
      })
      .catch(output => { reject(output) });
  });
}

function queryKeys_ethereum_ipfs(resourceHash, userName, privateKey) {
  let iv = false;
  let sessionKey = false;

  return new Promise((resolve, reject) => {
    return myContract.methods.queryResourceKeys(resourceHash).call()
    .then(keysID_bytes => {
      keysID = multihash_bytes2Hex_js(keysID_bytes);
      if (keysID == null ) {
        reject({
          'status': false,
          'value': 'ErrNullKey'
        });
      }

      ipfs.get(keysID, function (err, files) {
        if (!err) {
          const contents = files[0].content.toString('utf8');
          const contents_obj = JSON.parse(contents);

          // Get IV
          if ("iv" in contents_obj)
            iv = keys_bs64toHex_js(contents_obj['iv']);
          else
            reject({
              'status': false,
              'value': 'ErrNoIV'
            });

          if (userName in contents_obj) {
            const sessionKey_enc = keys_bs64toHex_js(contents_obj[userName]);
            try {
              sessionKey = decryptAsym_js(privateKey, sessionKey_enc);
            } catch (err) {
              reject({
                'status': false,
                'value': 'ErrAsymDec'
              });
            }
          } else {
            reject({
              'status': false,
              'value': 'ErrNoUser'
            })
          }

          resolve({'status': true, 'value': {'iv': iv, 'sk': sessionKey}});
        } else { reject({'status': false, 'value': 'ErrNoFile'}) };
      });
    });
  });
}

function errorCode2Human_js(error) {
  /*
   * Transforms error codes to human readable format
   * All final outputs (from "wrappers") should handle output errors using this
   */
  let output = error + ': ';
  switch(error) {
    case 'ErrAsymDec':
      output += 'Error decrypting session key. Is your private key correct?\n';
      break;
    case 'ErrNoIV':
      output += 'No IV found in key file\n';
      break;
    case 'ErrNoUser':
      output += 'User not in key file. Is user allowed?\n';
      break;
    case 'ErrNoFile':
      output += 'File not found\n';
      break;
    case 'ErrNullKey':
      output += 'Resource or its key do not exist\n';
      break;
    case 'ResourceExists':
      output += 'Resource already exists\n';
      break;
    case 'NoPrivKey':
      output +=
        'If iv/session key not given, private key must be stored locally\n';
      break;
    case 'ErrEnc':
      output += 'Error encrypting file\n';
      break;
    case 'ErrCheck':
      output += 'Resource does not exist or you are not the author\n';
      break;
    case 'NoRegSol':
      output += 'Resource does not exist or has no content';
      break;
  }
  return output;
}

function buildChatMessage() {
  // from updateKeysID_send2author to a standard model
}

/*
 * TODO:
 *    web3.utils.asciiToHex, ... wrappers
 *    queryUser(web3.eth.defaultAccount) -> save to variable (update on
 *    authentication) so that we dont have to query the blockchain every time
 */

/*
 * **************************************************************************
 *                             EXPORTS
 * **************************************************************************
 */
module.exports = {
  libraries : {
    ipfs: ipfs,
    web3: web3,
    bs58: bs58,
    ecies: ecies,
    OpenTimestamps
  },
  functions : {
    computeHash: computeHash_js,
    checkOwner: checkOwner_js,
    deepCopy: deepCopy_js,
    queryWid_inner: queryWid_inner_ethereum,
    queryWot_ipfs_inner: queryWot_ipfs_inner,
    verify_signature: verify_signature,
    verifySignature_fromAddress: verifySignature_fromAddress,
    pushWot_ipfs_inner: pushWot_ipfs_inner,
    sign: sign,
    multihash_bytes2Hex: multihash_bytes2Hex_js,
    multihash_hex2Bytes: multihash_hex2Bytes_js,
    getQueriedWot: getQueriedWot_js,
    queryPubKey: queryPubKey_ethereum,
    encryptSym: encryptSym_js,
    decryptSym: decryptSym_js,
    encryptAsym: encryptAsym_js,
    decryptAsym: decryptAsym_js,
    setWid: setWid_ethereum,
    updateKeysID: updateKeysID_ethereum_ipfs,
    generateSessionKey: generateSessionKey_js,
    generateIV: generateIV_js,
    keys_hex2bs64: keys_hex2bs64_js,
    keys_bs64toHex: keys_bs64toHex_js,
    modifyResourceWotHash: modifyResourceWotHash_ethereum,
    downloadResource: downloadResource_ipfs,
    readInputFile: readInputFile_js,
    uploadEncryptedResource: uploadEncryptedResource_ipfs,
    queryKeys: queryKeys_ethereum_ipfs,
    errorCode2Human: errorCode2Human_js
  },
  constants: {
    CONFIG: CONFIG,
    EMPTYADDR: EMPTYADDR,
    EMPTYB32: EMPTYB32,
    EMPTYPUBKEY: EMPTYPUBKEY,
    OWNERNAME: OWNERNAME,
    OWNERADDR: OWNERADDR
  },
  objects: {
    myContract: myContract,
    localWot: localWot,
    globalWot: globalWot,
    socket_gwot: socket_gwot
  }
}
