/*
 * **************************************************************************
 *                             IMPORTS
 * **************************************************************************
 */
const common = require('./common.js');
const web3 = common.libraries.web3;
const EMPTYADDR = common.constants.EMPTYADDR;
const checkOwner = common.functions.checkOwner;

/*
 * **************************************************************************
 *                             FUNTIONS
 * **************************************************************************
 */

function select_populate() {
  /*
   * Populates the authentication option menu with accounts read from geth
   * node
   */
  const current_div = document.getElementById('account');
  let select = current_div.getElementsByTagName('select')[0];
  web3.eth.getAccounts().then(account_list => {
    account_list.forEach(account => {
      let account_option = document.createElement('option');
      account_option.value = account;
      account_option.innerHTML = account;
      select.appendChild(account_option);
    });
    web3.eth.defaultAccount = account_list[0];
  });
  ui_disable(EMPTYADDR);
}

function ui_disable(address) {
  /*
   * Depending on if the user is the owner of the smart contract or not,
   * the administration or the user/resource/local wot tabs will be unblocked.
   *
   * Only if the user is the owner, it can perform administration tasks. This
   * is enforced from the smart contract side as well.
   */
  let sections = document.getElementsByTagName("section");
  let sections_array = Array.from(sections);

  if (address == EMPTYADDR) {
    sections_array.slice(1,5).forEach(section => {
      section.classList.add("disabled");
    });
  } else {
    /*
     * If contract not deployed correctly (example, if miners not interconnected)
     * An error occurs here. It asks if enough gas founds are available.
     * TODO: Research more and show error
     */
    // Why address is sometimes null at the beginning?
    //console.log(address);
    checkOwner(address).then(isOwner => {
      if (isOwner) {
        sections_array.slice(1,4).forEach(section => {
          section.classList.add("disabled");
        });
        sections_array[4].classList.remove("disabled");
      } else {
        sections_array.slice(1,4).forEach(section => {
          section.classList.remove("disabled");
        });
        sections_array[4].classList.add("disabled");
      }
    });
  }
}

function authenticate_geth() {
  /*
   * Authenticates to the ethereum geth node with the account selected on
   * the menu and the input password introduced.
   *
   * Important note:
   *  Requires running geth with --rpcapi "eth,net,web3,personal"
   */
  const unlockTime = 1500;
  const current_div = document.getElementById('authenticate');
  const password = current_div.getElementsByTagName('input')[0].value;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = "";

  const previous_div = document.getElementById('account');
  const select = previous_div.getElementsByTagName('select')[0];
  const tmp_account = select.options[select.selectedIndex].value;

  // Check account is selected and password given . This is not necessary but
  // helps understanding the error messages
  if (tmp_account === "Ethereum accounts"){
    current_pre.innerHTML +=
      "Select an account first to be able to authenticate";
    return false;
  }
  if (password.length === 0 ) {
    current_pre.innerHTML +=
      "Please enter a password to be able to authenticate";
    return false;
  }

  current_pre.innerHTML = '';
  web3.eth.personal.unlockAccount(
    tmp_account, password, unlockTime
  ).then(success => {
    web3.eth.defaultAccount = tmp_account;
    current_pre.innerHTML += success;
    ui_disable(web3.eth.defaultAccount);
  })
  .catch(error => {current_pre.innerHTML = error});
}

function authenticate_ethereum() {
  const current_div = document.getElementById('authenticate');
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  // Why window.web3 needed and not just web3?
  if (window.web3 !== undefined) {
    if (window.web3.currentProvider.isMetaMask === true) {
      ui_disable(web3.eth.defaultAccount);
      current_pre.innerHTML += 'Authentication performed by metamask';
    }
  } else {
    authenticate_geth();
    current_pre.innerHTML += 'Authentication performed by provider\n';
  }
}

function copyAddr2clipboard_js() {
  /*
   * Copy address to clipboard functionality.
   *
   * Useful as the address is sometimes necessary and writing it manually
   * is a pain.
   */
  const previous_div = document.getElementById('account');
  const select = previous_div.getElementsByTagName('select')[0];
  const address = select.options[select.selectedIndex].value;

  let tmp_textarea = document.createElement('textarea');
  tmp_textarea.value = address;
  //tmp_textarea.style.display = 'none';
  tmp_textarea.style.position = 'absolute';
  tmp_textarea.style.left = '-9999px';
  document.body.appendChild(tmp_textarea);
  tmp_textarea.select();
  document.execCommand('copy');
  document.body.removeChild(tmp_textarea);
}

/*
 * **************************************************************************
 *                             EXPORTS
 * **************************************************************************
 */
module.exports = {
  select_populate: select_populate,
  authenticate: authenticate_ethereum,
  copyAddr: copyAddr2clipboard_js
}
