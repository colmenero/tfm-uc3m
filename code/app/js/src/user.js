/* **************************************************************************
 *                             IMPORTS
 * **************************************************************************
 */
const common = require('./common.js');
const web3 = common.libraries.web3;
const myContract = common.objects.myContract;
const queryPubKey = common.functions.queryPubKey;
const readInputFile = common.functions.readInputFile;
const EMPTYADDR = common.constants.EMPTYADDR;
const EMPTYB32 = common.constants.EMPTYB32;

/*
 * **************************************************************************
 *                             FUNTIONS
 * **************************************************************************
 */

function createUser_ethereum() {
  /*
   * Important notes:
   *    - Requires mining.
   */
  let current_div = document.getElementById('createUser');
  const userName = current_div.getElementsByTagName('input')[0].value;
  const userName_bytes = web3.utils.asciiToHex(userName, 32);
  const userAddress = web3.eth.defaultAccount;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  // Check user does not exist beforehand
  myContract.methods.queryAddress(userName_bytes).call()
  .then(conditionAddress => {
    if (conditionAddress == EMPTYADDR) {

      // Check address does not have already associated user
      myContract.methods.queryUser(userAddress).call()
        .then(conditionName => {
          if (conditionName == EMPTYB32) {

            // Create user
            myContract.methods.createUser(userName_bytes, userAddress)
            .send({from: web3.eth.defaultAccount})
            .on('transactionHash', (hash) => {
              current_pre.innerHTML = 'Transaction Hash:\n' + hash;
              document.getElementById('createUser').classList.add('disabled');
            });

          } else {
            current_pre.innerHTML = "Address already exists\n";
          }
        });

    } else {
      current_pre.innerHTML = "User already exists\n";
    }
  });
}

function queryAddress_ethereum() {
  const current_div = document.getElementById('queryAddress');
  const userName = current_div.getElementsByTagName('input')[0].value;
  const userName_bytes = web3.utils.asciiToHex(userName, 32);
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  // TODO: Improve by reusing queryAddress()
  if (userName.length === 0 )
    myContract.methods.queryUser(web3.eth.defaultAccount).call()
    .then(user => {
      myContract.methods.queryAddress(user).call()
      .then(address => {
        current_pre.innerHTML =
          (address == EMPTYADDR) ? "User does not exist" : address;
      });
    });
  else
    myContract.methods.queryAddress(userName_bytes).call()
    .then(userAddress => {
      current_pre.innerHTML =
        (userAddress == EMPTYADDR) ? "User does not exist" : userAddress;
    });
}

function queryUser_ethereum() {
  const current_div = document.getElementById('queryUser');
  let userAddress = current_div.getElementsByTagName('input')[0].value;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  if (userAddress.length === 0 ) {
    userAddress = web3.eth.defaultAccount;
  }

  myContract.methods.queryUser(userAddress).call()
    .then(userName_bytes => {
      const userName = web3.utils.hexToUtf8(userName_bytes);
      current_pre.innerHTML =
        (userName_bytes == EMPTYB32) ? "Address does not exist" : userName;
    });
}

function deleteUser_ethereum() {
  /*
   * Important notes:
   *    - Requires mining.
   *    - It only deletes the user if it is itself: the address calling the
   *      function must be the same associated to the user.
   */
  let current_div = document.getElementById('deleteUser');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  const userAddress = web3.eth.defaultAccount;
  myContract.methods.queryUser(userAddress).call()
    .then(userName_bytes => {
      myContract.methods.deleteUser(userName_bytes)
      .send({from: web3.eth.defaultAccount})
      .on('transactionHash', (hash) => {
        current_pre.innerHTML = 'Transaction Hash:\n' + hash;
      });
    });

  // Enable create user
  let tmp = document.getElementById('createUser');
  tmp.classList.remove('disabled');
  let tmp_pre = tmp.getElementsByTagName('pre')[0];
  tmp_pre.innerHTML = '';
}

function queryPubKey_ethereum() {
  let current_div = document.getElementById('queryPubKey');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  const userName = current_div.getElementsByTagName('input')[0].value;
  const userName_bytes = web3.utils.asciiToHex(userName, 32);

  // TODO: Improve by reusing queryPubKey
  if (userName.length === 0 )
    myContract.methods.queryUser(web3.eth.defaultAccount).call()
    .then(username => {
      queryPubKey(username).then(pubKey => {
        current_pre.innerHTML = pubKey;
      })
    });
  else
    queryPubKey(userName_bytes).then(pubKey => {
      current_pre.innerHTML = pubKey;
    })
}

function changeAddress_ethereum() {
  /*
   * Important notes:
   *    - Requires mining.
   *
   */
  let current_div = document.getElementById('changeAddress');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  const newAddress = current_div.getElementsByTagName('input')[0].value;
  const userAddress = web3.eth.defaultAccount;
  myContract.methods.queryUser(userAddress).call()
  .then(userName_bytes => {
    myContract.methods.changeAddress(userName_bytes, newAddress)
      .send({from: web3.eth.defaultAccount})
      .on('transactionHash', (hash) => {
        current_pre.innerHTML = 'Transaction Hash:\n' + hash;
      });
  });
}

function changePubKey_ethereum() {
  /*
   * Important notes:
   *    - Requires mining.
   *
   */
  function changePubKey_main(value) {
    if (value.length != 128) {
      current_pre.innerHTML = 'Public Key must be 64 bytes';
      return false;
    }
    const userAddress = web3.eth.defaultAccount;

    myContract.methods.queryUser(userAddress).call()
    .then(userName_bytes => {
      const value0 = '0x' + value.slice(0, 64);
      const value1 = '0x' + value.slice(64);
      myContract.methods.setPubKey_half(userName_bytes, value0, false)
        .send({from: web3.eth.defaultAccount})
        .on('transactionHash', (hash1) => {
          myContract.methods.setPubKey_half(userName_bytes, value1, true)
            .send({from: web3.eth.defaultAccount})
            .on('transactionHash', (hash2) => {
              let msg = 'Two transactions (one for each half of public key)\n' +
                'Hash1: ' + hash1 + 'Hash2: ' + hash2;
              current_pre.innerHTML = msg;
            });
        });
    });
  }

  const current_div = document.getElementById('changePubKey');
  const written = current_div.getElementsByTagName('input')[0].value;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = "";

  // Read from file if no text input
  if (written.length === 0) {
    const file = current_div.getElementsByTagName('input')[1].files[0];
    readInputFile(file)
    .then(buffer => {
      const pubKey = buffer.toString('utf8');
      changePubKey_main(pubKey);
    });
  } else {
    changePubKey_main(written);
  };
}

function createUser_check_ethereum() {
  /*
   * An error appears here if contract address not valid.
   * TODO: Handle errors
   *
   * If tab == user -> check if user exists and if so, gray out div
   */
  const userAddress = web3.eth.defaultAccount;
  myContract.methods.queryUser(userAddress).call()
  .then(userName_bytes => {
    if (userName_bytes == EMPTYB32) {
      let tmp = document.getElementById('createUser');
      tmp.classList.remove('disabled');
      let current_pre = tmp.getElementsByTagName('pre')[0];
      current_pre.innerHTML = '';
    }
    else {
      let tmp = document.getElementById('createUser');
      tmp.classList.add('disabled');
      let current_pre = tmp.getElementsByTagName('pre')[0];
      current_pre.innerHTML = 'You are currently logged as ' +
        web3.utils.hexToUtf8(userName_bytes);
    }
  });
}

function storePrivateKey_js() {
  /*
   * store private key for automation
   *
   * INSECURE!!!
   *
   */
  const current_div = document.getElementById('storePrivKey');
  const written = current_div.getElementsByTagName('input')[0].value;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = "";

  let value = false;
  if (written.length === 0) {
    const file = current_div.getElementsByTagName('input')[1].files[0];
    readInputFile(file)
    .then(contents => {
        localStorage.setItem('dPKI_privateKey', contents.toString());
        current_pre.innerHTML = "Saved";
    });
  } else {
    value = written;
    localStorage.setItem('dPKI_privateKey', value);
    current_pre.innerHTML = "Saved";
  }
}

function showPrivateKey_js() {
  const current_div = document.getElementById('storePrivKey');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  const value = localStorage.getItem('dPKI_privateKey');
  if (value === null)
    current_pre.innerHTML = "Private key not stored";
  else
    current_pre.innerHTML = value;
}

/*
 * **************************************************************************
 *                             EXPORTS
 * **************************************************************************
 */
module.exports = {
  createUser: createUser_ethereum,
  queryAddress: queryAddress_ethereum,
  queryUser: queryUser_ethereum,
  queryPubKey: queryPubKey_ethereum,
  deleteUser: deleteUser_ethereum,
  changeAddress: changeAddress_ethereum,
  changePubKey: changePubKey_ethereum,
  createUser_check: createUser_check_ethereum,
  storePrivateKey: storePrivateKey_js,
  showPrivateKey: showPrivateKey_js
}
