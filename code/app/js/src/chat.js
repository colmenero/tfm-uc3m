const common = require('./common.js');
const web3 = common.libraries.web3;
const CONFIG = common.constants.CONFIG;
const myContract = common.objects.myContract;
const socket_gwot = common.objects.socket_gwot;
const verifySignature_fromAddress =
  common.functions.verifySignature_fromAddress;
const decryptSym = common.functions.decryptSym;
const decryptAsym = common.functions.decryptAsym;
const updateKeysID = common.functions.updateKeysID;
const keys_bs64toHex = common.functions.keys_bs64toHex;
const checkOwner = common.functions.checkOwner;
const readRegistry = common.functions.readRegistry;
const readEncryptedResource = common.functions.readEncryptedResource;
const queryKeys = common.functions.queryKeys;
const downloadResource = common.functions.downloadResource;
const uploadEncryptedResource = common.functions.uploadEncryptedResource;

/*
 *                             SOCKET.IO EVENTS
 * --------------------------------------------------------------------------
 */
socket_gwot.on('welcome', (message) => {
  console.log(message);
});

socket_gwot.on('author', function(msg) {
  if (msg['signed']['dest'] === web3.eth.defaultAccount) {
    let signature = msg['signature'];
    let signedData = msg['signed'];
    let origAddress = signedData['orig'];
    verifySignature_fromAddress(origAddress, signedData, signature)
    .then(verifiedSignature => { if (verifiedSignature) {
      handle_message(signedData);
    }})
  }
  // Else: There was a message for someone else
});

/*
 *                               FUNCTIONS
 * --------------------------------------------------------------------------
 */
function handle_message(data) {

  privateKey = localStorage.getItem('dPKI_privateKey');
  if (privateKey === null) {
    console.log('Private key not found');
    return false;
  }

  const iv = keys_bs64toHex(data['iv']);
  let sessionKey = false;
  const sessionKey_enc = keys_bs64toHex(data['sessionKey_enc']);
  try {
    sessionKey = decryptAsym(privateKey, sessionKey_enc);
  } catch (err) {
    console.log('Received message: Error decrypting session key.\n');
    console.log('Check your private key is stored correctly');
    return false;
  }

  const data_dec_buff = decryptSym(
    sessionKey, iv, Buffer.from(data['encrypted'], 'base64')
  );
  const data_dec = JSON.parse(data_dec_buff);

  switch(data_dec['action']) {
    case CONFIG.vars.chatEvents.updateKeysID:
      myContract.methods.queryUser(data['dest']).call()
      .then(userName_bytes => {
        const userName = web3.utils.hexToUtf8(userName_bytes);
        // updateKeysID can only be sent by owner
        checkOwner(data['orig']).then(isOwner => {
          if (isOwner)
            handle_updateKeysID(
              data_dec['resourceHash'], data_dec['allowedUsers'],
              userName, privateKey
            );
        });
      });
      break;

    case CONFIG.vars.chatEvents.getGWot:
      const current_div = document.getElementById('getResourceGwot');
      const current_pre = current_div.getElementsByTagName('pre')[0];
      current_pre.innerHTML = JSON.stringify(data_dec['allowedUsers']);
      break;
  }
}

function handle_updateKeysID(resourceHash, allowedUsers,
  userName, privateKey) {
  /*
   * TODO: send results to owner
   */
  function handle_error(error, current_pre) {
    switch (error) {
      case 'ErrNullKey':
        // No previous key found. Generate one
        updateKeysID(resourceHash, allowedUsers)
        .then(output_update => {
          const iv_new = output_update['value']['iv'];
          const sk_new = output_update['value']['sk'];
          current_pre.innerHTML += '\nKey updated for resourceHash: ' + resourceHash;
          current_pre.innerHTML += '\nNew IV: ' + iv_new;
          current_pre.innerHTML += '\nNew Session Key: ' + sk_new;
        })
        .catch(output_update => {current_pre.innerHTML += output_update});
        break;
      case 'NoRegSol':
        current_pre.innerHTML +=
          '\nNo Registry found in Solidity. Is resource content uploaded?';
        break;
      default:
        current_pre += error;
    }
  }
  const current_div = document.getElementById('updateKeysID_author');
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  if (!allowedUsers) {
    current_pre.innerHTML += 'Resource not in global table';
    return false;
  }

  queryKeys(resourceHash, userName, privateKey)
  .then(output_keys => {
    const iv_old = output_keys['value']['iv'];
    const sk_old = output_keys['value']['sk'];
    const version = 0; // always latest
    updateKeysID(resourceHash, allowedUsers)
    .then(output_update => {
      const iv_new = output_update['value']['iv'];
      const sk_new = output_update['value']['sk'];
      current_pre.innerHTML += '\nKey updated for resourceHash: ' + resourceHash;
      current_pre.innerHTML += '\nNew IV: ' + iv_new;
      current_pre.innerHTML += '\nNew Session Key: ' + sk_new;
      downloadResource(resourceHash, version, sk_old, iv_old)
      .then(output_resource => {
        const resource = output_resource['value']['content'];
        uploadEncryptedResource(resourceHash, resource, sk_new, iv_new)
        .then(output_upload => {current_pre.innerHTML += '\nResource updated'})
        .catch(output_upload => {
          handle_error(output_upload['value'], current_pre);
        });
      })
      .catch(output_resource => {
        handle_error(output_resource['value'], current_pre);
      });
    })
    .catch(output_update => {
      handle_error(output_update['value'], current_pre)
    })
  })
  .catch(output_keys => {handle_error(output_keys['value'], current_pre)});
}
