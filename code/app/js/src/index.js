const index = new Promise ((resolve, reject) => {
  /*
  * **************************************************************************
  *                             INDEX.JS
  *
  * The application is composed of a six of tabs:
  *   Authentication, user management, resource management, local wot,
  *   administration and legend
  * Each tab is wrapped around a HTML section tag and represent a group of
  * similar actions.
  *
  * Javascript code is organized in the same way. For each tab we have a js
  * source file that is imported here to join all functionality.
  *
  * At the end, the same functionality is going to be available to the webapp.
  * Functions that the webapp needs are exported through a set of interfaces
  * with the same name as the tabs.
  *
  * This was not a promise at the beginning but became necessary to handle
  * metamask
  * **************************************************************************
  */
  window.addEventListener('load', async () => {
    const common = require('./common.js');
    const authentication = require('./authentication.js');
    const user = require('./user.js');
    const resource = require('./resource.js');
    const local = require('./local.js');
    const admin = require('./admin.js');
    const chat = require('./chat.js');

    const web3 = common.libraries.web3;
    const myContract = common.objects.myContract;

    // Populates the authentication menu with accounts read from geth node
    document.addEventListener(
      'DOMContentLoaded', authentication.select_populate()
    );
    // At the begining show only first tab - authentication
    document.addEventListener('DOMContentLoaded', switchTab(0));
    document.addEventListener('DOMContentLoaded', addSwitchingEvents());

    /*
    *                             TAB MANAGEMENT
    * --------------------------------------------------------------------------
    */

    function switchTab(activeTab) {
      /*
      * Make the input tab (activeTab) active and the rest inactive
      *
      * Tabs can be accessed through the main menu of the application so that when
      * a tab link is clicked on the menu, only it should become active
      * (accessible) and the rest should hide. This is achieved by changing css
      * classes.
      */
      let sections = document.getElementsByTagName('section');
      let sections_array = Array.from(sections);

      sections_array.forEach(section => {
        section.classList.remove('tab-active');
        section.classList.add('tab-inactive');
      });

      sections_array[activeTab].classList.remove('tab-inactive');
      sections_array[activeTab].classList.add('tab-active');

      if (activeTab === 1) {
        user.createUser_check();
      }
    }

    function addSwitchingEvents() {
      /*
      * If menu is clicked -> its associated tab, and only that one, should
      * become active.
      */
      main_nav = document.getElementById('main_nav');
      main_as = main_nav.getElementsByTagName('a');
      for(let idx=0; idx<main_as.length; idx++) {
        main_as[idx].onclick = function() {return switchTab(idx)};
      }
    }

    /*
    *                      COLLAPSE INTERMEDIATE STEPS
    * --------------------------------------------------------------------------
    */
    let collapse_buttons = document.getElementsByClassName("collapse");
    let collapse_content = document.getElementsByClassName("collapse_content");

    for (let i = 0; i < collapse_buttons.length; i++) {
      collapse_buttons[i].addEventListener("click", function() {
        if (collapse_content[i].style.display === "block")
          collapse_content[i].style.display = "none";
        else
          collapse_content[i].style.display = "block";
      });
    }

    /*
    *                             EVENTS
    * --------------------------------------------------------------------------
    * events.modifiedUserWot -> Logs when a WoT is pushed to IPFS
    */
    myContract.events.modifiedUserWot({}, (error, data) => {
      if (error) {
        // This is fired as well if connection to miner fails
        console.log("Log event error: " + error);
      } else {
        console.log("Log event: \n\tName of the Solidity function: " + data.event);
        let userName_bytes = data.returnValues[0];
        let userName = web3.utils.hexToUtf8(userName_bytes);
        console.log("\tUsername updating Wot: " + userName);
        let wot_bytes = data.returnValues[1];
        let wot_hex = common.functions.multihash_bytes2Hex(wot_bytes);
        console.log("\tCID of WoT: " + wot_hex);
      }
    });

    resolve({
      common: common,
      authentication: authentication,
      user: user,
      resource: resource,
      local: local,
      admin: admin,
      switchTab: switchTab
    })
  });
});

module.exports = index;
