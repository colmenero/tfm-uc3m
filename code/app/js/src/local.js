/* **************************************************************************
 *                             IMPORTS
 * **************************************************************************
 */
var common = require('./common.js');
const bs58 = common.libraries.bs58;
const web3 = common.libraries.web3;
const ipfs = common.libraries.ipfs;
var myContract = common.objects.myContract;
var localWot = common.objects.localWot;
const EMPTYADDR = common.constants.EMPTYADDR;
const EMPTYB32 = common.constants.EMPTYB32;
const deepCopy = common.functions.deepCopy;
const queryWid_inner = common.functions.queryWid_inner;
const queryWot_ipfs_inner = common.functions.queryWot_ipfs_inner;
const pushWot_ipfs_inner = common.functions.pushWot_ipfs_inner;
const getQueriedWot = common.functions.getQueriedWot;
const sign = common.functions.sign;
const setWid = common.functions.setWid;
const computeHash = common.functions.computeHash;
const socket_gwot = common.objects.socket_gwot;
const OWNERADDR = common.constants.OWNERADDR;
const multihash_bytes2Hex = common.functions.multihash_bytes2Hex;
const queryKeys = common.functions.queryKeys;
const keys_bs64toHex = common.functions.keys_bs64toHex;
const decryptAsym = common.functions.decryptAsym;
const encryptSym = common.functions.encryptSym;
const decryptSym = common.functions.decryptSym;
const verifySignature_fromAddress = common.functions.verifySignature_fromAddress;
const errorCode2Human = common.functions.errorCode2Human;

/*
 * **************************************************************************
 *                             FUNTIONS
 * **************************************************************************
 */

function queryWot_js() {
  let current_div = document.getElementById('queryWot_local');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const output = JSON.stringify(localWot, undefined, 2);
  current_pre.innerHTML = output;
}

function addWot_js() {
  const current_div = document.getElementById('addWot');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const userName = current_div.getElementsByTagName('input')[1].value;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  // Check inputs
  if (!userName || !resourceHash) {
    current_pre.innerHTML += "Input can't be blank";
    return false;
  }
  if ((resourceHash.slice(0,2) !== "0x") || (resourceHash.length !== 66)) {
    // TODO: Check hexadecimal
    current_pre.innerHTML += "Resource identifier must start with 0x " +
      "and be a string of 64 hexadecimal characters (32 Bytes)";
    return false;
  }

  if (localWot[resourceHash] == undefined) {
    localWot[resourceHash] = [];
  }
  // Check node does not already exist
  if (localWot[resourceHash].indexOf(userName) < 0) {
    localWot[resourceHash].push(userName);
  }
  current_pre.innerHTML += "Local entry added\n";

  if (socket_gwot.connected) {
    const privateKey = localStorage.getItem('dPKI_privateKey');
    if (privateKey === null) {
      current_pre.innerHTML +=
        "Save private key if you want to notify users of WoT update";
      return false;
    }
    notifyUsers(privateKey, 'add', resourceHash, userName)
    .then(output_notification => {
      current_pre.innerHTML += output_notification['value'];
    })
    .catch(output_notification => {
      current_pre.innerHTML += "Notification error: " +
        errorCode2Human(output_notification['value']);
    })
  }
}

function findWot_js() {
  /*
   * If userName is undefined, return list of nodes with access
   * If userName is defined, return index indicating position in list
   * (if -1, it does not exist)
   */
  const current_div = document.getElementById('findWot');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const userName = current_div.getElementsByTagName('input')[1].value;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  if (resourceHash in localWot) {
    if (userName == null || userName == "")
      current_pre.innerHTML +=
        "List of nodes for that resource: " + localWot[resourceHash];
    else
      current_pre.innerHTML += "Index of that user in the resource list: " +
        localWot[resourceHash].indexOf(userName);
  }
  else
    current_pre.innerHTML += "Resource does not exist";
}

function delWot_js() {
  const current_div = document.getElementById('delWot');
  const current_pre = current_div.getElementsByTagName('pre')[0];
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const userName = current_div.getElementsByTagName('input')[1].value;
  current_pre.innerHTML = '';

  if (userName == null || userName == "") {
    delete localWot[resourceHash];
    userName = "everything"; // message in notification to users
    current_pre.innerHTML += "Resource entry deleted from local WoT\n";
  } else {
    const index = localWot[resourceHash].indexOf(userName);
    if (index >= 0) {
      localWot[resourceHash].splice(index, 1);
      current_pre.innerHTML += "User deleted from resource entry\n";
    } else {
      current_pre.innerHTML += "User not found in resource entry\n";
      return false;
    }
  }

  // Delete resource if no users (last user was just deleted)
  if (localWot[resourceHash].length === 0 )
    delete localWot[resourceHash];

  // Notify other users if channel available
  if (socket_gwot.connected) {
    const privateKey = localStorage.getItem('dPKI_privateKey');
    if (privateKey === null)
      current_pre.innerHTML +=
        "Save private key if you want to notify users of WoT update\n";
    else {
      notifyUsers(privateKey, 'delete', resourceHash, userName)
      .then(output_notification => {
        current_pre.innerHTML += output_notification['value'];
      })
      .catch(output_notification => {
        current_pre.innerHTML += "Notification error: " +
          errorCode2Human(output_notification['value']);
      })
    }
  }
}

function pushWot_ipfs() {
  let current_div = document.getElementById('pushWot');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const userAddress = web3.eth.defaultAccount;
  const wot = deepCopy(localWot);
  const flag_est = current_div.getElementsByTagName('input')[0].checked;
  const flag_pub = current_div.getElementsByTagName('input')[1].checked;
  pushWot_ipfs_inner(userAddress, current_pre, wot, flag_est, flag_pub);
}

function queryWid_ethereum() {
  let current_div = document.getElementById('queryWid');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  const userAddress = web3.eth.defaultAccount;
  myContract.methods.queryUser(userAddress).call()
    .then(userName_bytes => {
      const userName = web3.utils.hexToUtf8(userName_bytes);
      queryWid_inner(userName).then(output => {
        current_pre.innerHTML = output;
      });
    });
}

function queryWot_ipfs() {
  let current_div = document.getElementById('queryWot_ipfs');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const userAddress = web3.eth.defaultAccount;
  myContract.methods.queryUser(userAddress).call()
    .then(userName_bytes => {
      const userName = web3.utils.hexToUtf8(userName_bytes);
      queryWid_inner(userName).then(fileHash => {
        if (fileHash !== null) {
          queryWot_ipfs_inner(fileHash, userName, current_pre);
        } else {
          current_pre.innerHTML += "CID of WoT not found in Ethereum\n";
        }
      });
    });
}

function saveWot_js(flag) {
  var queriedWot = getQueriedWot();
  localWot = deepCopy(queriedWot);
}

function setWid_ethereum() {
  let current_div = document.getElementById('setWid_admin');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const value = current_div.getElementsByTagName('input')[0].value;
  const flag_pub = current_div.getElementsByTagName('input')[1].checked;

  const userAddress = web3.eth.defaultAccount;
  myContract.methods.queryUser(userAddress).call()
    .then(userName_bytes => {
      const userName = web3.utils.hexToUtf8(userName_bytes);
      setWid(userName, value, flag_pub);
      current_pre.innerHTML +=
        "New Wid should have changed. Check using the Query Wot ID function";
    });
}

function estimateWotHash_js() {
  /*
   * Based on current local WoT, estimates the hash that the WoT would have
   * (if entries in global WoT would match)
   */
  const current_div = document.getElementById('estimateWotHash');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let output = false;
  let resourceHash = current_div.getElementsByTagName('input')[0].value;
  if(resourceHash in localWot) {
    let resourceWot = localWot[resourceHash];
    let wotHash = computeHash(resourceWot);
    output = "Estimated WOT hash for given resource is: " + wotHash;
  } else {
    output = "Resource not found in local WOT";
  }
  current_pre.innerHTML = output;
}

function notifyUsers(privateKey, action, resourceHash, userName) {
  /*
    * TODO: Handle errors
    *       Common function with bundle.resource.queryKeysID_ethereum
    */
  return new Promise((resolve, reject) => {
    const origAddress = web3.eth.defaultAccount;
    return myContract.methods.queryUser(origAddress).call()
    .then(origName_bytes => {
      const origName = web3.utils.hexToUtf8(origName_bytes);
      return queryKeys(resourceHash, origName, privateKey)
      .then(output_keys => {
        const iv  = output_keys['value']['iv'];
        const sessionKey  = output_keys['value']['sk'];
        data = {'action': action, 'value': userName};
        const data_str = JSON.stringify(data);
        const data_enc = encryptSym(sessionKey, iv, data_str);
        const data2sign = {
          'dest': resourceHash,
          'orig': origAddress,
          'orig_name': origName,
          'encrypted': data_enc.toString('base64')
        }

        return sign(JSON.stringify(data2sign), origAddress)
        .then(signature => {
          const message = {'signature': signature, 'signed': data2sign};
          socket_gwot.emit("localWot", message);
          resolve({'status': true, 'value': action + ' notified'});
        });
      })
      .catch(output_keys => reject(output_keys));
    })
    .catch(output => {reject(output)});
  });
}

socket_gwot.on('localWot', function (msg) {
  /*
   * Needed here because accesses localWot
   */
  if (msg['signed']['dest'] in localWot) {
    const signature = msg['signature'];
    const signedData = msg['signed'];
    const origAddress = signedData['orig'];

    privateKey = localStorage.getItem('dPKI_privateKey');
    if (privateKey === null) {
      return false;
    }

    verifySignature_fromAddress(origAddress, signedData, signature)
    .then(verifiedSignature => { if (verifiedSignature) {
      const resourceHash = signedData['dest'];
      const data_enc = signedData['encrypted'];

      myContract.methods.queryResourceKeys(resourceHash).call()
      .then(keysID_bytes => {
        keysID = multihash_bytes2Hex(keysID_bytes);
        if (keysID !== null ) {
          ipfs.get(keysID, function (err, files) {
            if (!err) {
              const contents = files[0].content.toString('utf8');
              const contents_obj = JSON.parse(contents);
              const myAddress = web3.eth.defaultAccount;
              myContract.methods.queryUser(myAddress).call()
              .then(myName_bytes => {
                const myName = web3.utils.hexToUtf8(myName_bytes);
                if (myName in contents_obj) {
                  const iv = keys_bs64toHex(contents_obj["iv"]);
                  const sessionKey_enc = keys_bs64toHex(contents_obj[myName]);
                  const sessionKey = decryptAsym(privateKey, sessionKey_enc);
                  const data_dec = JSON.parse(
                    decryptSym(sessionKey, iv, Buffer.from(data_enc, 'base64'))
                  );
                  console.log(
                    "Local Wot Update:" + resourceHash + ': ' +
                    signedData['orig_name'] + ' ' +
                    data_dec['action'] + "s " + data_dec['value']
                  );
                }
              });
            }
          });
        }
      });
    }});
  }
});


/*
 * **************************************************************************
 *                             EXPORTS
 * **************************************************************************
 */
module.exports = {
  queryWot: queryWot_js,
  addWot: addWot_js,
  findWot: findWot_js,
  delWot: delWot_js,
  pushWot: pushWot_ipfs,
  queryWid: queryWid_ethereum,
  queryWot_ipfs: queryWot_ipfs,
  saveWot: saveWot_js,
  estimateWotHash: estimateWotHash_js,
  setWid: setWid_ethereum
}
