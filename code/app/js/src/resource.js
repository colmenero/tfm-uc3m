var common = require('./common.js');
const web3 = common.libraries.web3;
const ipfs = common.libraries.ipfs;
const ecies = common.libraries.ecies;
const OpenTimestamps = common.libraries.OpenTimestamps;
const bs58 = common.libraries.bs58;
const socket_gwot = common.objects.socket_gwot;
const myContract = common.objects.myContract;
const decryptAsym = common.functions.decryptAsym;
const sign = common.functions.sign;
const multihash_bytes2Hex = common.functions.multihash_bytes2Hex;
const multihash_hex2Bytes = common.functions.multihash_hex2Bytes;
const encryptSym = common.functions.encryptSym;
const decryptSym = common.functions.decryptSym;
const keys_bs64toHex = common.functions.keys_bs64toHex;
const EMPTYB32 = common.constants.EMPTYB32;
const EMPTYADDR = common.constants.EMPTYADDR;
const OWNERADDR = common.constants.OWNERADDR;
const checkOwner = common.functions.checkOwner;
const modifyResourceWotHash = common.functions.modifyResourceWotHash;
const readInputFile = common.functions.readInputFile;
const downloadResource = common.functions.downloadResource;
const uploadEncryptedResource = common.functions.uploadEncryptedResource;
const queryKeys = common.functions.queryKeys;
const updateKeysID = common.functions.updateKeysID;
const errorCode2Human = common.functions.errorCode2Human;

/*
 * **************************************************************************
 *                             FUNTIONS
 * **************************************************************************
 */

function notifyOwner(resourceHash, action) {
  /*
    * TODO: Encrypt message.
    *       So far this message is not going to be encrypted for simplicity.
    */
  const userAddress = web3.eth.defaultAccount;
  OWNERADDR.then(ownerAddress => {
    const data2sign = {
      'dest': ownerAddress,
      'orig': userAddress,
      'action': action,
      'resourceHash': resourceHash
    }
    sign(JSON.stringify(data2sign), userAddress).then(signature => {
      const message = {
        'signature': signature,
        'signed': data2sign
      }
      socket_gwot.emit('resource', message);
    });
  });
}

function createResource_ethereum(resourceHash) {
  return new Promise((resolve, reject) => {
    myContract.methods.queryResourceAuthor(resourceHash).call()
    .then(author_bytes => {
      const author = web3.utils.hexToUtf8(author_bytes);
      if (author_bytes == EMPTYB32) {
        myContract.methods.createResource(resourceHash)
        .send({from: web3.eth.defaultAccount})
        .on('transactionHash', (transactionHash) => {
          if (socket_gwot.connected)
            notifyOwner(resourceHash, 'add');
          resolve({
            'status': true,
            'value': transactionHash
          });
        });
      } else {
        reject({
          'status': false,
          'value': 'ResourceExists'
        })
      }
    });
  });
}

function createResource_wrapper() {
  /*
   * Important notes:
   *    - Requires mining.
   */
  const current_div = document.getElementById('createResource');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  createResource_ethereum(resourceHash)
  .then(output => {
    current_pre.innerHTML += 'Transaction Hash:\n' + output['value'];
  })
  .catch(output => {
    current_pre.innerHTML += errorCode2Human(output['value']);
  });
}

function queryAuthor_ethereum() {
  const current_div = document.getElementById('queryAuthor');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  myContract.methods.queryResourceAuthor(resourceHash).call()
    .then(author_bytes => {
      const author = web3.utils.hexToUtf8(author_bytes);
      const err_msg = "Resource doesn't exist or doesn't have associated user";
      current_pre.innerHTML =
        (author_bytes == EMPTYB32) ? err_msg : author;
    });
}

function modifyAuthor_ethereum() {
  const current_div = document.getElementById('modifyAuthor');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const newAuthor = current_div.getElementsByTagName('input')[1].value;
  const newAuthor_bytes = web3.utils.asciiToHex(newAuthor, 32);
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  myContract.methods.queryAddress(newAuthor_bytes).call()
    .then(newAuthorAddress => {
      if (newAuthorAddress != EMPTYADDR) {
        myContract.methods.modifyResourceAuthor(resourceHash, newAuthor_bytes)
        .send({from: web3.eth.defaultAccount})
        .on('transactionHash', (hash) => {
          current_pre.innerHTML = 'Transaction Hash:\n' + hash;
        });
      } else {
        current_pre.innerHTML = "New author does not exist\n";
      }
    });
}

function deleteResource_ethereum() {
  /*
   * Important notes:
   *    - Requires mining.
   */
  const current_div = document.getElementById('deleteResource');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  myContract.methods.deleteResource(resourceHash)
  .send({from: web3.eth.defaultAccount})
  .on('transactionHash', (hash) => {
    current_pre.innerHTML = 'Transaction Hash:\n' + hash;
    if (socket_gwot.connected)
      notifyOwner(resourceHash, 'delete');
  });
}

function queryWotHash_ethereum() {
  /*
   * Given a resource, get from Ethereum its WoT hash (hash of its global WoT
   * entry - of its allowed users)
   */
  const current_div = document.getElementById('queryWotHash');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  myContract.methods.queryResourceWot(resourceHash).call()
    .then(wotHash => {
      err_msg = "Resource does not exist/does not have wot";
      current_pre.innerHTML =
        (wotHash == EMPTYB32) ? err_msg : wotHash;
    });
}

function queryKeysID_ethereum() {
  /*
   * Given a resource, get from Ethereum the IPFS CID identifier for its key
   * file
   */
  const current_div = document.getElementById('queryKeysID');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let resourceHash = current_div.getElementsByTagName('input')[0].value;
  myContract.methods.queryResourceKeys(resourceHash).call()
    .then(keysID_bytes => {
      err_msg = "Resource does not exist/does not have key file";
      keysID = multihash_bytes2Hex(keysID_bytes);
      current_pre.innerHTML =
        (keysID == null) ? err_msg : keysID;
    });
}

function queryKeys_wrapper() {
  /*
   * Given a resource...
   *    - Get IPFS CID identifier for its key file (from Ethereum)
   *    - Read its key file searching for current account username (from IPFS)
   *    - If found, decrypt associated encrypted session key with the specified
   *      private key. The result is the session key used to encrypt resource
   */
  const current_div = document.getElementById('queryKeys');
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const privateKey = localStorage.getItem('dPKI_privateKey');
  if (privateKey === null) {
    current_pre.innerHTML = "Private key is needed. Save it first.";
    return false;
  }

  const userAddress = web3.eth.defaultAccount;
  myContract.methods.queryUser(userAddress).call()
  .then(userName_bytes => {
    const userName = web3.utils.hexToUtf8(userName_bytes);
    queryKeys(resourceHash, userName, privateKey)
    .then(output => {
      current_pre.innerHTML += "ResourceHash: " + resourceHash +
        "\nIV: " + output['value']['iv'] +
        "\nSession Key: " + output['value']['sk'];
    })
    .catch(output => {
      current_pre.innerHTML += errorCode2Human(output['value'])
    });
  });
}

function estimateResourceID_js(buffer) {
  function arrayBuffer2Hex (buffer) {
      /*
      * Reference:
      *   https://stackoverflow.com/questions/40031688/javascript-arraybuffer-to-hex
      */
      return Array
          .from (new Uint8Array (buffer))
          .map (b => b.toString (16).padStart (2, "0"))
          .join ("");
  }
  return new Promise((resolve, reject) => {
    // This crypto is the default js, not the node module!!
    // Note: cannot be used in unsecure environments (!= https/localhost)
    const hash_promise = crypto.subtle.digest('SHA-256', buffer);
    hash_promise.then(hash => {
      const output = '0x' + arrayBuffer2Hex(hash);
      resolve(output);
    });
  });
}

function estimateResourceID_wrapper() {
  /*
   * Read input file and compute hash
   *
   * Hash is computed using crypto default module and then transformed from
   * arrayBuffer to an hexadecimal string that gets output to the user.
   */
  const current_div = document.getElementById('estimateResourceID');
  const current_pre = current_div.getElementsByTagName('pre')[0];
  const file = current_div.getElementsByTagName('input')[0].files[0];
  current_pre.innerHTML = '';

  readInputFile(file).then(contents => {
    estimateResourceID_js(contents).then(ID => {
      current_pre.innerHTML = ID;
    });
  })
  .catch(contents => {
      current_pre.innerHTML = "Error reading file";
  });
}

function saveFile(filename, bytes) {
  /*
   * Download the given bytes as a file with filename name
   *
   * - Used to save the OpenTimestamp proof
   * - It creates a new link to a file made from input bytes and simulates
   *   clicking it
   */
  var blob = new Blob([bytes]);
  var link = document.createElement("a");
  link.href = window.URL.createObjectURL(blob);
  link.download = filename;
  link.style.visibility = 'hidden';
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}

async function retrieveTimeProof_openTS() {
  /*
   * Given a resource hash and a filename, generate proof and download it
   * with the specified filename.
   */
  const current_div = document.getElementById('retrieveTimeProof');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let hash = current_div.getElementsByTagName('input')[0].value;
  let filename = current_div.getElementsByTagName('input')[1].value;
  const hash_buffer = Buffer.from(hash,'hex');
  const detached = OpenTimestamps.DetachedTimestampFile
    .fromHash(new OpenTimestamps.Ops.OpSHA256(), hash_buffer);

  try {
    await OpenTimestamps.stamp(detached);
    const fileOts = detached.serializeToBytes();
    const infoResult = OpenTimestamps.info(detached);
    current_pre.innerHTML = infoResult;
    saveFile(filename, fileOts);
  } catch(err) {
    console.log(err);
  }
}

function verifyTimeProof_openTS() {
  /*
   * Given a hash and a proof file, verifies that the last one is correct
   *
   * If it is, it means that the resource was timestamped at the specified
   * date.
   */
  const current_div = document.getElementById('verifyTimeProof');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let hash = current_div.getElementsByTagName('input')[0].value;
  const hash_buffer = Buffer.from(hash,'hex');
  const detached = OpenTimestamps.DetachedTimestampFile
    .fromHash(new OpenTimestamps.Ops.OpSHA256(), hash_buffer);

  let fileList = current_div.getElementsByTagName('input')[1].files;
  let fileReader = new FileReader();
  if (fileReader && fileList && fileList.length) {
    fileReader.readAsArrayBuffer(fileList[0]);
    fileReader.onload = function () {
      const fileOts = Buffer.from(fileReader.result);
      const detachedOts = OpenTimestamps.DetachedTimestampFile
        .deserialize(fileOts);

      OpenTimestamps.verify(detachedOts, detached).then(results => {
        if(Object.keys(results).length === 0){
          current_pre.innerHTML = "Pending attestation";
        } else {
          Object.keys(results).forEach(key => {
            date_unixus = results[key].timestamp;
            date = new Date(date_unixus*1000);
            current_pre.innerHTML = key + " attests data existed as of " +
              date;
          });
        }
      }).catch( err => {console.log("Bad attestation" + err)});
    }
  }
}

function upgradeTimeProof_openTS() {
  /*
   * The timestamping is frankly, quite slow. It needs some time in order
   * to work. In case of problems, try upgrading the proof.
   */
  const current_div = document.getElementById('verifyTimeProof');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let fileList = current_div.getElementsByTagName('input')[1].files;
  let fileReader = new FileReader();
  if (fileReader && fileList && fileList.length) {
    fileReader.readAsArrayBuffer(fileList[0]);
    fileReader.onload = function () {
      const fileOts = Buffer.from(fileReader.result);
      const detachedOts = OpenTimestamps.DetachedTimestampFile
        .deserialize(fileOts);
      OpenTimestamps.upgrade(detachedOts).then(changed =>{
          if(changed){
              current_pre.innerHTML = "Timestamp upgraded\n";
              const upgradedOts = detachedOts.serializeToBytes()
              const upgradedHex = bytesToHex(upgradedOts)
              current_pre.innerHTML += upgradedHex;
          } else {
              current_pre.innerHTML = "Timestamp not changed\n";
          }
      })
    }
  }
}

function checkInputKeys_updown_js(resourceHash, iv, sessionKey) {
  return new Promise((resolve, reject) => {
    if (iv.length === 0 || sessionKey === 0) {
      const privateKey = localStorage.getItem('dPKI_privateKey');
      if (privateKey === null) {
        reject({
          'status': false,
          'value': 'NoPrivKey'
        });
      }
      const userAddress = web3.eth.defaultAccount;
      return myContract.methods.queryUser(userAddress).call()
      .then(userName_bytes => {
        const userName = web3.utils.hexToUtf8(userName_bytes);
        return queryKeys(resourceHash, userName, privateKey)
        .then(output => {
          iv = output['value']['iv'];
          sessionKey = output['value']['sk'];
          resolve({
            'status': true,
            'value': {
              'iv': iv,
              'sk': sessionKey
            }
          });
        })
        .catch(output => {
          switch (output['value']) {
            case 'ErrNullKey':
              /*
              * Resource does not have an associated key file yet.
              * One must be generated. Allowed users set to just author.
              */
              allowedUsers = [userName];
              return updateKeysID(resourceHash, allowedUsers)
              .then(output_keys =>
                resolve(output_keys)
              )
              .catch(output_keys => reject(output_keys));
              break;
            default:
              reject(output);
          }
        });
      });
      // TODO: Catch excepcion if user does not exist
    } else {
      resolve({
        'status': true,
        'value': {
          'iv': iv,
          'sk': sessionKey
        }
      });
    }
  });
}

function uploadResource_all(resourceHash, contents, iv_opt, sessionKey_opt,
  flag_enc, flag_reg)
{
  return new Promise((resolve, reject) => {
    checkInputKeys_updown_js(resourceHash, iv_opt, sessionKey_opt)
    .then(output_keys => {
      const iv = output_keys['value']['iv'];
      const sessionKey = output_keys['value']['sk'];
      return uploadEncryptedResource(resourceHash, contents, sessionKey, iv)
      .then(output => {
        resolve(output);
      })
      .catch(output => {
        reject(output);
      });
    })
    .catch(output_keys => {
      reject(output_keys);
    })
  });
}

async function uploadResource_wrapper() {
  const current_div = document.getElementById('uploadResource');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const file = current_div.getElementsByTagName('input')[1].files[0];
  let filename = current_div.getElementsByTagName('input')[2].value;
  filename = filename ? filename : 'encrypted.enc';
  const iv_opt = current_div.getElementsByTagName('input')[3].value;
  const sessionKey_opt = current_div.getElementsByTagName('input')[4].value;
  const flag_enc = current_div.getElementsByTagName('input')[5].checked;
  const flag_reg = current_div.getElementsByTagName('input')[6].checked;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  const contents = await readInputFile(file);
  if (contents === false) {
    current_pre.innerHTML += "Error reading file\n";
    return false;
  }
  uploadResource_all(resourceHash, contents, iv_opt, sessionKey_opt)
  .then(output_keys => {
    if (flag_enc)
      saveFile(filename, Uint8Array.from(output_keys['encrypted']));
    if (flag_reg)
      saveFile('RegistryOfChanges', Uint8Array.from(output_keys['content']));
    current_pre.innerHTML += 'New registry CID: ' + output_keys['value'];
  })
  .catch(output_keys => {
    current_pre.innerHTML += errorCode2Human(output_keys['value']);
  });
}


function downloadResource_wrapper() {
  // Initialization
  const current_div = document.getElementById('downloadResource');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  let filename = current_div.getElementsByTagName('input')[1].value;
  filename = filename ? filename : 'decrypted.dec';
  const iv_opt = current_div.getElementsByTagName('input')[2].value;
  const sessionKey_opt = current_div.getElementsByTagName('input')[3].value;
  const version = current_div.getElementsByTagName('input')[4].value;
  const flag_reg = current_div.getElementsByTagName('input')[5].checked;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  checkInputKeys_updown_js(resourceHash, iv_opt, sessionKey_opt)
  .then(output_keys => {
    const iv = output_keys['value']['iv'];
    const sessionKey = output_keys['value']['sk'];
    downloadResource(resourceHash, version, sessionKey, iv)
    .then(output_resource => {
      if(flag_reg)
        saveFile(
          'RegistryOfChanges',
          Uint8Array.from(Buffer.from(output_resource['value']['registry']))
        );
      saveFile(filename, output_resource['value']['content']);
    })
    .catch(output_resource => {
      // TODO: handle in upper catch
      current_pre.innerHTML += errorCode2Human(output_resource['value']);
    });
  })
  .catch(output_keys => {
    current_pre.innerHTML += errorCode2Human(output_keys['value']);
  });
}

function getResourceGwot_js() {
  const current_div = document.getElementById('getResourceGwot');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  notifyOwner(resourceHash, 'getGWot');
}

function updateResourceGwot_ethereum() {
  const current_div = document.getElementById('getResourceGwot');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  const gwot_str = current_pre.innerHTML;
  if (gwot_str !== "") {
    const gwot = JSON.parse(current_pre.innerHTML);
    modifyResourceWotHash(resourceHash, gwot)
    .then(output => {
      current_pre.innerHTML = output['wotHash'];
    });
  }
}

function updateKeysID_ethereum() {
  /*
   * TODO: Notify back to user
   */
  const current_div = document.getElementById('updateKeysID_author');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  const current_pre = current_div.getElementsByTagName('pre')[0];
  notifyOwner(resourceHash, 'updateKeys');
}

function startResource() {
  const current_div = document.getElementById('startResource');
  const current_pre = current_div.getElementsByTagName('pre')[0];
  const file = current_div.getElementsByTagName('input')[0].files[0];
  current_pre.innerHTML = '';

  readInputFile(file).then(contents => {
    estimateResourceID_js(contents).then(resourceHash => {
      current_pre.innerHTML += 'Resource Identifier: ' + resourceHash + '\n\n';
      createResource_ethereum(resourceHash)
      .then(output => {
        current_pre.innerHTML += 'New Resource Found. Transaction Hash:\n' +
          output['value'] + '\n\n';
        uploadResource_all(resourceHash, contents, '', '')
        .then(output_keys => {
          current_pre.innerHTML += 'New registry CID: ' + output_keys['value'];
        })
        .catch(output_keys => {
          current_pre.innerHTML += output_keys['value'];
          if (output_keys['value'] === 'ErrAuthor')
            current_pre.innerHTML += '\nTo upload content click submit again '
              + 'once creation transaction is finished!';
        });
      })
      .catch(output => {
        switch(output['value']) {
          case 'ResourceExists':
            current_pre.innerHTML += 'Resource already exists. Updating.\n';
            uploadResource_all(resourceHash, contents, '', '')
            .then(output_keys => {
              current_pre.innerHTML += 'New registry CID: ' +
                output_keys['value'];
            })
            .catch(output_keys => {
              current_pre.innerHTML += errorCode2Human(output_keys['value']);
            });
            break;
        }
      });
    });
  })
  .catch(contents => {
      current_pre.innerHTML = "Error reading file";
  });
}

/*
 * **************************************************************************
 *                             EXPORTS
 * **************************************************************************
 */
module.exports = {
  createResource: createResource_wrapper,
  queryAuthor: queryAuthor_ethereum,
  modifyAuthor: modifyAuthor_ethereum,
  deleteResource: deleteResource_ethereum,
  queryWotHash: queryWotHash_ethereum,
  queryKeysID: queryKeysID_ethereum,
  queryKeys: queryKeys_wrapper,
  estimateResourceID: estimateResourceID_wrapper,
  retrieveTimeProof: retrieveTimeProof_openTS,
  verifyTimeProof: verifyTimeProof_openTS,
  upgradeTimeProof: upgradeTimeProof_openTS,
  uploadResource: uploadResource_wrapper,
  downloadResource: downloadResource_wrapper,
  updateKeysID: updateKeysID_ethereum,
  getResourceGwot: getResourceGwot_js,
  updateResourceGwot: updateResourceGwot_ethereum,
  startResource: startResource
}
