/* **************************************************************************
 *                             IMPORTS
 * **************************************************************************
 */
const common = require('./common.js');
const web3 = common.libraries.web3;
const ipfs = common.libraries.ipfs;
const ecies = common.libraries.ecies;
const bs58 = common.libraries.bs58;
const socket_gwot = common.objects.socket_gwot;
const CONFIG = common.constants.CONFIG;
const OWNERNAME = common.constants.OWNERNAME;
const EMPTYB32 = common.constants.EMPTYB32;
const EMPTYPUBKEY = common.constants.EMPTYPUBKEY;
var myContract = common.objects.myContract;
var globalWot = common.objects.globalWot;
const deepCopy = common.functions.deepCopy;
const queryWid_inner = common.functions.queryWid_inner;
const queryWot_ipfs_inner = common.functions.queryWot_ipfs_inner;
const pushWot_ipfs_inner = common.functions.pushWot_ipfs_inner;
const checkOwner = common.functions.checkOwner;
const verify_signature = common.functions.verify_signature;
const verifySignature_fromAddress = common.functions.verifySignature_fromAddress;
const computeHash = common.functions.computeHash;
const multihash_bytes2Hex = common.functions.multihash_bytes2Hex;
const getQueriedWot = common.functions.getQueriedWot;
const updateKeysID = common.functions.updateKeysID;
const encryptAsym = common.functions.encryptAsym;
const encryptSym = common.functions.encryptSym;
const queryPubKey = common.functions.queryPubKey;
const generateSessionKey = common.functions.generateSessionKey;
const generateIV = common.functions.generateIV;
const sign = common.functions.sign;
const keys_hex2bs64 = common.functions.keys_hex2bs64;
const OWNERADDR = common.constants.OWNERADDR;
const modifyResourceWotHash = common.functions.modifyResourceWotHash;

/*
 * **************************************************************************
 *                             FUNTIONS
 * **************************************************************************
 */

function queryWid_ethereum() {
  /*
   * TODO:
   *   - Add checkOwner. Currently enforced by smartcontract (not important)
   */
  let current_div = document.getElementById('queryWid_admin');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const userName = current_div.getElementsByTagName('input')[0].value;
  queryWid_inner(userName).then(output => {
    current_pre.innerHTML = output;
  });
}

function queryWot_ipfs() {
  /*
   * TODO:
   *   - Add checkOwner. Currently enforced by smartcontract (not important)
   */
  let current_div = document.getElementById('queryWot_ipfs_admin');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const userName = current_div.getElementsByTagName('input')[0].value;
  queryWid_inner(userName).then(fileHash => {
    if (fileHash !== null)
      queryWot_ipfs_inner(fileHash, userName, current_pre);
  });
}

function queryGWot() {
  let current_div = document.getElementById('queryGWot');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const output = JSON.stringify(globalWot, undefined, 2);
  current_pre.innerHTML = output;
}

function queryGWot_ipfs() {
  let current_div = document.getElementById('queryGWot_ipfs');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  queryWid_inner(OWNERNAME).then(GWid_bs58 => {
    console.log(GWid_bs58);
    if (GWid_bs58 != null)
      queryWot_ipfs_inner(GWid_bs58, OWNERNAME, current_pre);
  });
}

function saveWot_js(flag) {
  var queriedWot = getQueriedWot();
  globalWot = deepCopy(queriedWot);
}


function updateGWot_queryAllNodes_js(threshold) {
  /*
   * Alternative to js_updateGWot_queryOnlyWotNodes
   * Read its doc notes to understand.
   */
  /*
   * Change globalWot -> affects addWot, findWot, deleteWot
   * globalWot = {
   *  resourceHash: [{user: occurrence}, {user: occurrence}],
   *  resourceHash2: [{user: occurrence}, {user: occurrence}]
   *  ...
   *  }
   *
   * for each file in wot (each local Wot)
   *     read resource
   *     if resource not in globalWot_occurrences, add it
   *     for each user of the resource
   *         if user not int resource, add it
   *         increment occurrence
   */
};

function updateGWot_queryOnlyWotNodes_js(threshold_percentage, current_pre) {
  /*
   * Update Global Wot based on queries to nodes already present in current wot
   *
   * Steps:
   *    1. For each resource on the current global Wot:
   *        1.1 read every local wot (every user) currently associated with it
   *        1.2 for every user that the local wot relates with the resource:
   *           1.2.1 increment one the occurrence
   *    2. update global wot with users that meet threshold (occurrence larger)
   *
   * Functions:
   *    1. -> js_updateGWot_queryOnlyWotNodes() -> THIS ONE
   *    1.1 -> updateFromLocals_resource()
   *    1.2 -> searchLocalWot()
   *    2 -> meetsWot()
   *
   * Notes:
   *    - This function updates based on current global wot. New resources must
   *      be pushed to the global wot with an initial node (e.g. author) so
   *      that they can be queried. Otherwise they will not be pushed from
   *      local to global Wot, as they are never included in global wot to
   *      begin with.
   *      Solution: updateGlobalWot_queryAllNodes(). Read data from all local
   *      nodes. Problem: the whole network would have influence.
   *
   * TODO:
   *    - If no user or its local wot found -> do not exit with error, continue
   *      ignoring their data. Problem: Promise.all should continue even with
   *      error, doesn't happen by default.
   */

  function meetsWot(threshold, resourceHash, occurrences) {
    /*
     * Function 2
     * ----------
     * For each user in occurrences, assigns to global Wot only if value
     * larger than threshold
     *
     * Sort at the end to keep consistency
     */
    if (typeof threshold != 'number') {
      console.log('Threshold must be number. Set to 0 temporarily');
      threshold = 0;
    }
    newGWot_resource = [];
    let users = Object.keys(occurrences);
    users.forEach(user => {
      if (occurrences[user] >= threshold) {newGWot_resource.push(user);}
    });
    return newGWot_resource.sort();
  }

  function searchLocalWot(userName, contents, resourceHash, occurrences) {
   /*
    * Function 1.2
    * ------------
    * For each local wot, read allowed users of resourceHash.
    * For each user, increment occurrence counter
    */
    const lines = contents.split('\n');
    const signature = lines[0];
    const wot_str = lines[1];
    verify_signature(userName, wot_str, signature)
      .then(success => {
        console.log("Signature verification of %s wot: %s", userName, success);
      });
    wot = JSON.parse(wot_str);

    if (!wot[resourceHash]) {
      console.log(
        'User %s does not contain %s in its local wot', userName, resourceHash
      );
    } else {
      let userList = wot[resourceHash];
      userList.forEach(user => {
        occurrences[user] = ++occurrences[user] || 1;
      });
    }
    return occurrences;
  }

  function updateFromLocals_resource(threshold, resourceHash, current_pre) {
    /*
     * Function 1.1
     * ------------
     * For every user associated to resource in global Wot, read its local Wot
     * and call 1.2
     *
     * Fucking promises, this gets complicated. I am sure there is a better way
     * of handling this
     */

    /*
     * Users -> Wids
     * Type:
     *   String[] -> Promises[return String]
     */
    let promises_wid = [];
    let users = globalWot[resourceHash];
    users.forEach(user => {
      let promise_wid = queryWid_inner(user);
      promises_wid.push(promise_wid);
    });

    let promises_files = [];
    return Promise.all(promises_wid).then(fileHashes => {
      /*
      * Get files from  Wids
      *
      * Promises[return String] -> Promises[return File]
      *
      * What if there is an error reading the WoT? could i return an error
      * variable without catching the whole promise? Otherwise the next
      * Promise.all() will always catch.....
      */
      fileHashes.forEach(fileHash  => {
        let promise_file = new Promise((resolve, reject) => {
          if (fileHash !== null) {
            ipfs.get(fileHash)
            .then(files => resolve(files))
            // .catch(resolve(null)) --> does not work
            .catch(error => current_pre.innerHTML +=
                '\nIPFS local Wot not found. Error: ' + error);
          } else {resolve(null)}
        });
        promises_files.push(promise_file);
      });

      /*
       * Handle contents
       *
       * Promises[return File] -> Promise[return String[]]
       *
       * Note: Assume CID is file, not path.
       *       Doing promise => files[0]
       *       Instead of promises -> files -> file (running files.forEach)
       *
       * TODO: What if CID of Wot is pointing to a resource valid in ipfs but
       *       not with proper format (is not created by this program)?
       *       We could have a try block surrounding the contents inside then
       *       if condition. Check into it.
       */
      let occurrences = {};
      return Promise.all(promises_files).then(promises => {
        promises.forEach((files, idx) => {
          if (files !== null) {
            let contents = files[0].content.toString('utf8');
            occurrences = searchLocalWot(
              users[idx], contents, resourceHash, occurrences
            );
          }
        });
        newGWot_resource = meetsWot(threshold, resourceHash, occurrences);
        return newGWot_resource;
      })
    })
  }

  /*
   * Function 1.
   * -----------
   * First block:
   *    For each resource on current Wot, call 1.1
   *    Result is a promise containing allowed users (new wot)
   * Second block:
   *    When all promises are done, save to output in a dictionary mapping
   *    resource hashes to the list of their allowed users
   */
  let promises_resources = [];
  let globalWot_resources = Object.keys(globalWot);
  globalWot_resources.forEach(resourceHash => {
    nUsers = globalWot[resourceHash].length;
    threshold = Math.round(threshold_percentage*nUsers);
    promise_resource = updateFromLocals_resource(
      threshold, resourceHash, current_pre);
    promises_resources.push(promise_resource);
  })

  let output = {};
  return Promise.all(promises_resources).then(promises => {
    promises.forEach((resourceWot, idx) => {
      let resourceHash = globalWot_resources[idx];
      output[resourceHash] = resourceWot;
    });
    return output;
  });
}

function updateGWot_js() {
  let current_div = document.getElementById('updateGWot');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  let threshold = current_div.getElementsByTagName('input')[0].value;
  current_pre.innerHTML = '';
  updateGWot_queryOnlyWotNodes_js(threshold, current_pre)
    .then(output => {if(output) globalWot = deepCopy(output)});
}

function addGWot_js() {
  let current_div = document.getElementById('addGWot');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let success = false;
  let resourceHash = current_div.getElementsByTagName('input')[0].value;
  let userName = current_div.getElementsByTagName('input')[1].value;
  if (!userName || !resourceHash) {
    success = "Input can't be blank";
  } else {
    if (globalWot[resourceHash] == undefined) {
      globalWot[resourceHash] = [];
    }
    // Check node does not already exist
    if (globalWot[resourceHash].indexOf(userName) < 0) {
      globalWot[resourceHash].push(userName);
    }
    success = true;
  }
  current_pre.innerHTML = success;
}

function delGWot_js() {
  let current_div = document.getElementById('delGWot');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let resourceHash = current_div.getElementsByTagName('input')[0].value;
  let userName = current_div.getElementsByTagName('input')[1].value;

  if (!resourceHash) {
    current_pre.innerHTML = "Input resource can't be blank";
    return false;
  }

  if (userName == null || userName == "") {
    if (globalWot[resourceHash] !== undefined) {
      delete globalWot[resourceHash];
      current_pre.innerHTML = true;
      return true;
    } else {
      current_pre.innerHTML = 'Resource to delete not found';
      return false;
    }
  } else {
    let index = globalWot[resourceHash].indexOf(userName);
    if (index >= 0) {
      globalWot[resourceHash].splice(index, 1);
      current_pre.innerHTML = true;
      return true;
    } else {
      current_pre.innerHTML = 'User/resource to delete not found';
      return false;
    }
  }
}

function pushGWot_ipfs_inner(userAddress, current_pre, onlyCID, flag_public) {
  const wot = deepCopy(globalWot);
  current_pre.innerHTML = '';

  checkOwner(userAddress).then(isOwner => {
    if (isOwner) {
      pushWot_ipfs_inner(userAddress, current_pre, wot, onlyCID, flag_public);
    } else {
      console.log('You need to be owner');
    }
  });
}

function pushGWot_ipfs() {
  const userAddress = web3.eth.defaultAccount;
  const current_div = document.getElementById('pushGWot');
  const flag_est = current_div.getElementsByTagName('input')[0].checked;
  const flag_pub = current_div.getElementsByTagName('input')[1].checked;
  let current_pre = current_div.getElementsByTagName('pre')[0];

  pushGWot_ipfs_inner(userAddress, current_pre, flag_est, flag_pub);
};

function queryWotHash_ethereum() {
  let current_div = document.getElementById('queryWotHash_admin');
  const resourceHash = current_div.getElementsByTagName('input')[0].value;
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  myContract.methods.queryResourceWot(resourceHash).call()
    .then(wotHash => {
      err_msg = "Resource does not exist/does not have wot";
      current_pre.innerHTML =
        (wotHash == EMPTYB32) ? err_msg : wotHash;
    });
}

function estimateWotHash_js() {
  /*
   */
  let current_div = document.getElementById('estimateWotHash_admin');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let output = false;
  let resourceHash = current_div.getElementsByTagName('input')[0].value;
  if(resourceHash in globalWot) {
    let resourceWot = globalWot[resourceHash];
    let wotHash = computeHash(resourceWot);
    output = "Estimated WOT hash for given resource is: " + wotHash;
  } else {
    output = "Resource not found in global WOT";
  }
  current_pre.innerHTML = output;
}

function updateWotHash_ethereum() {
  const current_div = document.getElementById('updateWotHash');
  const current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';
  const resourceHash = current_div.getElementsByTagName('input')[0].value;

  // Requirement 1: Resource must be in global Wot
  if(!(resourceHash in globalWot)) {
    current_pre.innerHTML += "Resource not found in global WOT\n";
    return false;
  }

  // Requirement 2: Resource must be registered in ethereum
  myContract.methods.queryResourceAuthor(resourceHash).call()
  .then(author_bytes => {
      if (author_bytes == EMPTYB32) {
        current_pre.innerHTML += "Resource does not exist. Check Ethereum\n";
        return false;
      } else {
        // Main function. Runs if requirements met
        const resourceWot = globalWot[resourceHash];
        modifyResourceWotHash(resourceHash, resourceWot)
        .then(output => {
          current_pre.innerHTML += 'Transaction Hash:\n' +
            output['transactionHash'] +
            "\nNew WOT hash for given resource is: " + output['wotHash'];
        });
      }
  })
}

function updateKeysID_wrapper() {
  /*
   * Given a resource, update its keys
   *
   * This means retrieving allowed users, create new session key (TODO: reuse
   * previous?), encrypt session key with public keys, save into file and push
   * it to IPFS, update ethereum to point to new file.
   */
  let current_div = document.getElementById('updateKeysID');
  let current_pre = current_div.getElementsByTagName('pre')[0];
  current_pre.innerHTML = '';

  let resourceHash = current_div.getElementsByTagName('input')[0].value;
  updateKeysID_checkRequirements(resourceHash)
  .then(output => {
    current_pre.innerHTML = 'CID of Key file for ' +
      output['value']['resourceHash'] + ':\n ' + output['value']['CID'];
  })
  .catch(output => {
    current_pre.innerHTML = "Error updating Keys\n" + output['value'];
  });
}

function updateKeysID_checkRequirements(resourceHash) {
  /*
   *
   */
  function updateKeysID_send2author(resourceHash, allowedUsers, author_bytes) {
    /*
     * TODO:
     *    - Handle errors
     *    - Get keys back from author and print them on screen.
     *      That way we also have confirmation that update process worked.
     *      What if it fails? Nothing/generate by owner as if no chat.
     *
     */
    return new Promise((resolve, reject) => {
      queryPubKey(author_bytes).then(pubKey => {
        if (pubKey  === EMPTYPUBKEY) {
          const author = web3.utils.hexToUtf8(author_bytes);
          reject({
            'success': false,
            'value': 'No pubkey found for: ' + author + '\n'
          });
        } else {
          const iv = generateIV(); // hex string
          const pubKey_noprefix = pubKey.slice(2);
          const sessionKey = generateSessionKey(); // hex string
          const sessionKey_enc = encryptAsym(pubKey_noprefix, sessionKey);

          myContract.methods.queryAddress(author_bytes).call()
          .then(authorAddress => {
            data = {
              'action': CONFIG.vars.chatEvents.updateKeysID,
              'resourceHash': resourceHash,
              'allowedUsers': allowedUsers
            };
            const data_str = JSON.stringify(data);
            const data_enc = encryptSym(sessionKey, iv, data_str); // buffer

            const ownerAddress = web3.eth.defaultAccount;
            const data2sign = {
              'dest': authorAddress,
              'orig': ownerAddress,
              'sessionKey_enc': keys_hex2bs64(sessionKey_enc),
              'iv': keys_hex2bs64(iv),
              'encrypted': data_enc.toString('base64')
            }

            sign(JSON.stringify(data2sign), ownerAddress).then(signature => {
              const message = {
                'signature': signature,
                'signed': data2sign
              };
              socket_gwot.emit('broadcast', message);
              resolve({
                'success': true,
                'value': {
                  'resourceHash': resourceHash,
                  'CID': 'Data sent to author, who should upload keys briefly\n'
                }
              });
            });
          });
        }
      });
    });
  }

  function updateKeysID_checkChatServer(resourceHash, author_bytes) {
    /*
    * If there is no server to provide the out-of-band communication between
    * nodes, fall back to basic solution -> the owner updates. Problem of this
    * solution: at some point in time the owner is able to read the session key.
    *
    * TODO: Implement out-of-band communication using p2p protocol such as
    * bitmessage.
    */
    const resourceWot = globalWot[resourceHash];

    if (socket_gwot.connected) {
      console.log('Update done by author');
      return new Promise(function (resolve, reject) {
        updateKeysID_send2author(resourceHash, resourceWot, author_bytes)
        .then(output => {
          resolve(output);
        })
        .catch(output => {
          reject(output);
        });
      });
    }else {
      console.log('Update done locally by owner');
      return new Promise(function (resolve, reject) {
        updateKeysID(resourceHash, resourceWot)
        .then(output => {
          resolve(output);
        })
        .catch(output => {
          reject(output);
        });
      });
    }
  }

  return new Promise(function (resolve, reject) {
    // Requirement 1: Resource must be in global Wot
    if(!(resourceHash in globalWot)) {
      reject({
        'success': false,
        'value': 'Resource not found in global WOT'
      });
    }

    // Requirement 2: Resource must be registered in ethereum
    myContract.methods.queryResourceAuthor(resourceHash).call()
    .then(author_bytes => {
      if (author_bytes == EMPTYB32) {
        reject({
          'success': false,
          'value': 'Resource does not exist. Check Ethereum'
        });
      } else {
        // Main function. Runs if requirements met
        updateKeysID_checkChatServer(resourceHash, author_bytes)
        .then(output => {
          resolve(output);
        })
        .catch(output => {
          reject(output);
        });
      }
    })
  });
}

function updateAll_all() {
  /*
   * Runs the process of updating the global Wot, but...
   *    - Instead of directly writing new values, we copy it to temporal object
   *    - Compare the old and new global Wots. When there is a difference its
   *      means the keys and Wot hash must be updated.
   */
  function arr_diff(input1, input2) {
    /*
     * Find if array differs: false if same
     */
    if(input1.length != input2.length) {
      return true;
    } else {
      input1.forEach(elem => {
        if (input2.indexOf(elem) < 0) {
          return true;
        }
      });
    }
    return false;
  }

  function updateWotHash_tmp(resourceHash, current_pre) {
    /*
     * This function can be probably integrated better into the whole updateAll
     */
    // Requirement 1: Resource must be in global Wot
    if(!(resourceHash in globalWot)) {
      current_pre.innerHTML += "Resource not found in global WOT\n";
      return false;
    }

    // Requirement 2: Resource must be registered in ethereum
    myContract.methods.queryResourceAuthor(resourceHash).call()
      .then(author_bytes => {
          if (author_bytes == EMPTYB32) {
            current_pre.innerHTML += "Resource does not exist. Check Ethereum\n";
            return false;
          } else {
            // Main function. Runs if requirements met
            let resourceWot = globalWot[resourceHash];
            modifyResourceWotHash(resourceHash, resourceWot)
            .then(output => {
              current_pre.innerHTML += 'Transaction Hash:\n' +
                output['transactionHash'] +
                "\nNew WOT hash for given resource is: " +
                output['wotHash'];
            });
          }
      })
  }

  const current_div = document.getElementById('updateAll');
  let threshold = current_div.getElementsByTagName('input')[0].value;
  const flag_pub = current_div.getElementsByTagName('input')[1].checked;
  const userAddress = web3.eth.defaultAccount;
  let current_pre = current_div.getElementsByTagName('pre')[0];

  updateGWot_queryOnlyWotNodes_js(threshold, current_pre)
    .then(globalWot_new => {
      if(globalWot_new) {  // if no errors updating

        // if resource list of allowed users differ... update keys and hash
        for (var resourceHash in globalWot_new) {
          if (arr_diff(globalWot_new[resourceHash], globalWot[resourceHash])) {

            updateKeysID_checkRequirements(resourceHash)
            .then(output => {
              current_pre.innerHTML +=
                'CID of Key file for ' + output['value']['resourceHash'] +
                ':\n ' + output['value']['CID'];
            })
            .catch(output => {
              current_pre.innerHTML += "Error updating Keys\n" + output['value'];
            });

            updateWotHash_tmp(resourceHash, current_pre);
          }
        }

        // update globalWot
        globalWot = deepCopy(globalWot_new);

        // push to ipfs
        pushGWot_ipfs_inner(userAddress, current_pre, false, flag_pub);
      } else {
        current_pre += 'Error updating global Wot';
      }
    });
}

socket_gwot.on('resource', function(msg) {
  /*
   * Why here and not in chat.js? -> Need to modify globalWot
   *
   * TODO: Message encrypted.
   *       That way we can use a common event (such as admin) and differentiate
   *       on the action.
   */
  OWNERADDR.then(ownerAddress => {
    if (
      (ownerAddress === msg['signed']['dest']) &&
      (ownerAddress === web3.eth.defaultAccount)
    ){
      let signature = msg['signature'];
      let signedData = msg['signed'];
      let origAddress = signedData['orig'];
      verifySignature_fromAddress(origAddress, signedData, signature)
      .then(verifiedSignature => { if (verifiedSignature) {
        const resourceHash = signedData['resourceHash'];
        switch(signedData['action']) {
          case 'delete':
            if (globalWot[resourceHash] != undefined)
              delete globalWot[resourceHash];
            break;
          case 'add':
            myContract.methods.queryUser(origAddress).call()
            .then(origName_bytes => {
              const origName = web3.utils.hexToUtf8(origName_bytes);
              if (globalWot[resourceHash] == undefined)
                globalWot[resourceHash] = [origName];
            });
            break;
          case 'getGWot':
            getGWot_handler(origAddress, resourceHash);
            break;
          case 'updateKeys':
            updateKeysID_checkRequirements(resourceHash)
            .then(output => {
            })
            .catch(output => {
            });
            break;
        }
      }})
    }
    // Else: There was a message for someone else (shouldnt - admin just one)
  });
})

function getGWot_handler(address, resourceHash) {
  /*
   * Why here and not in chat.js? -> Need to access globalWot
   *
   * Handle errors.
   *  E.g what if username or pubkey not successful?
   */
  myContract.methods.queryUser(address).call()
  .then(origName_bytes => {
    const origName = web3.utils.hexToUtf8(origName_bytes);
    // Note: before i was using indexOf to find if included. Unify
    if (
      (typeof globalWot[resourceHash] !== 'undefined') &&
      globalWot[resourceHash].includes(origName)
    ) {
      const value2return = globalWot[resourceHash];
      const iv = generateIV(); // hex string
      const sessionKey = generateSessionKey(); // hex string

      queryPubKey(origName_bytes).then(pubKey => {
        const pubKey_noprefix = pubKey.slice(2);
        const sessionKey_enc = encryptAsym(pubKey_noprefix, sessionKey);

        data = {
          'action': CONFIG.vars.chatEvents.getGWot,
          'resourceHash': resourceHash,
          'allowedUsers': value2return
        };
        const data_str = JSON.stringify(data);
        const data_enc = encryptSym(sessionKey, iv, data_str); // buffer

        const ownerAddress = web3.eth.defaultAccount;
        const data2sign = {
          'dest': address,
          'orig': ownerAddress,
          'sessionKey_enc': keys_hex2bs64(sessionKey_enc),
          'iv': keys_hex2bs64(iv),
          'encrypted': data_enc.toString('base64')
        }

        sign(JSON.stringify(data2sign), ownerAddress).then(signature => {
          const message = {
            'signature': signature,
            'signed': data2sign
          };
          socket_gwot.emit('broadcast', message);
        });
      });
    }
  });
}

/*
 * **************************************************************************
 *                             EXPORTS
 * **************************************************************************
 */
module.exports = {
  queryWid: queryWid_ethereum,
  queryWot_ipfs: queryWot_ipfs,
  queryGWot: queryGWot,
  queryGWot_ipfs: queryGWot_ipfs,
  saveWot: saveWot_js,
  updateGWot: updateGWot_js,
  addGWot: addGWot_js,
  delGWot: delGWot_js,
  pushGWot: pushGWot_ipfs,
  queryWotHash: queryWotHash_ethereum,
  estimateWotHash: estimateWotHash_js,
  updateWotHash: updateWotHash_ethereum,
  updateKeysID: updateKeysID_wrapper,
  updateAll: updateAll_all
}
