pragma solidity ^0.5.0;

contract Crypto {
    /*
     * @title A title that should describe the contract
     * @author Author of the contract
     * @notice Explain to a user what the function does
     * @dev VERY IMPORTANT: ALWAYS USE PURE/VIEW HERE!!!
     *      Otherwise, we could expose the private key or other secret
     *      information to the block chain.
     *      --> Why not deploy this better at client side? less
     *      "self-contained" program but less risky and less cost
     *      --> seems only safe solution. If we ever executed a function from
     *      another contract that called one of these, it would get recorded in
     *      the transaction that called the original function.
     */

    function verifySchnorr() pure external {
        /*
         * @notice Generates Schnorr data
         * @dev Replace by official function when available!!
         * @param parameter's name Documents parameter
         * @return Document return type
         */
    }
}
