pragma solidity ^0.5.0;

import "./Users.sol";
import "./Resources.sol";

/*
 * @title Distributed PKI
 * @author Luis Colmenero Sendra
 * @notice This contract is the main(), the entry door. It is a placeholder to:
 *         - Users.sol: Contract containing user-specific functions.
 *         - Resources.sol: Contract containing resource-specific functions.
 *         - Storage.sol: Contract containing data and functions used both for
 *           users and resources.
 */
contract PKI is Users, Resources {

}
