pragma solidity ^0.5.0;

contract Storage {

    struct Multihash {
        /*
         * @notice Multihash structure. Used in IPFS.
         *
         * @dev IPFS uses Multihash.
         *      If expressed in hexadecimal: The first byte is the hash
         *      function and the second is length. The last two are the value.
         *      So far, only possible value for the first two bytes is 0x1220
         *      (sha2:0x12, 256bits:0x20). Both bytes can be stored as uint8.
         *      As the length of the value part can only be 256 bits (32
         *      bytes), it can be stored on a bytes32 type.
         *      Ethereum storage slots are 32 bytes (256bits). Storing the
         *      algorithm and the length together inside a struct compacts them
         *      into one slot, saving space.
         */
      uint8 alg;
      uint8 len;
      bytes32 val;
    }

    /* ************************************************************
     *                  USER RELATED
     * ************************************************************
     *
     * Users defined by their username (bytes32)
     *   users[userName] returns user structure
     *
     * @notice
     *      Important restriction: Maximum length of userName is 32 characters!
     *      String encoding is UTF8 (variable), so not always 32 is max.
     *
     *      Requires conversion from/to string to bytes (done in app)
     *
     * @dev why bytes32?
     *      It uses less gas and in a consistent way (string gas expenses
     *      change as they are dinamically sized)
     */

    struct PubKey {
        /*
         * @notice Public key structure.
         *         Separated as two halves so that they fit in bytes32:
         *         for efficiency.
         */
        bytes32 half0;
        bytes32 half1;
    }

    struct User {
        address ethereumAddress;
        PubKey pubkey;
        Multihash wot;
    }

    mapping(bytes32 => User) internal users;
    mapping(address => bytes32) internal addr2user;

    modifier existsUser(bytes32 userName, bool invert) {
        bool success;
        bool exists = users[userName].ethereumAddress != address(0x0);
        invert? success = !exists: success = exists;
        require(success == true);
        _;
    }

    modifier existsAddress(address userAddress, bool invert) {
        bool success;
        bool exists = addr2user[userAddress] != bytes32(0);
        invert? success = !exists: success = exists;
        require(success == true);
        _;
    }

    /* ************************************************************
     *                  RESOURCE RELATED
     * ************************************************************
     * The wot value of a resource is the hash of global_wot[resource]
     * The wot value of an user is its local wot's IPFS CID: As local
     * wots are published to IPFS, a multihash value of them is
     * as a reference and is used to retrieve it later.
     */
     struct Resource {
        bytes32 author;
        bytes32 wot;
        Multihash keys;
        Multihash file;
    }

    mapping(bytes32 => Resource) internal resources;

    modifier existsResource(bytes32 resourceHash, bool invert) {
        bool success;
        bool exists = resources[resourceHash].author != bytes32(0);
        invert? success = !exists: success = exists;
        require(success == true);
        _;
    }

    /* ************************************************************
     *                  OWNERSHIP RELATED
     * ************************************************************
     * Apart from internal constructor, not used a lot.
     */
    bytes32 public ownerName;
    uint UserFee = 0 ether;
    uint ResourceFee = 0 ether;

    constructor () internal {
        /*
         * The owner name is and should be hard-coded.
         * In ascii it is the string "owner".
         */

        /*
         * Create User owner.
         * Should be same definition as createUser but with specific
         * user and repeting code, as it is not yet visible
         */
        ownerName = bytes32(0x6f776e6572000000000000000000000000000000000000000000000000000000);
        users[ownerName].ethereumAddress = msg.sender;
        addr2user[msg.sender] = ownerName;

        setMultihash(users[ownerName].wot, bytes32(0x0));
    }

    modifier onlyOwner(bool invert) {
        bool success;
        bool isOwner = (msg.sender == users[ownerName].ethereumAddress);
        invert? success = !isOwner: success = isOwner;
        require(success == true);
        _;
    }

    function setNewUserFee(uint fee) external
    onlyOwner(false)
    returns (bool) {
        UserFee = fee;
        return true;
    }

    function setNewResourceFee(uint fee) external
    onlyOwner(false)
    returns (bool) {
        ResourceFee = fee;
        return true;
    }

    function changeOwnership(address newAddress) external
    onlyOwner(false)
    returns (bool) {
        address oldAddress = users[ownerName].ethereumAddress;
        users[ownerName].ethereumAddress = newAddress;
        addr2user[newAddress] = ownerName;
        delete addr2user[oldAddress];
        return true;
    }

    /* ************************************************************
     *                  OTHER
     * ************************************************************
     */
    function setMultihash(Multihash storage multihash, bytes32 value) internal
    returns (bool) {
        multihash.alg = 18; //0x12
        multihash.len = 32; //0x20
        multihash.val = value;

        return true;
    }

}

