pragma solidity ^0.5.0;

import "./Storage.sol";

contract Users is Storage {
    /*
     * @title Title
     * @author Luis Colmenero Sendra
     * @notice Inherits from Storage.sol and will be inherited by PKI.sol
     *         Contains user-specific functions: create/delete users and
     *         query/set their attributes.
     */

    event NewUser(bytes32 userName, address userAddress);
    event DeletedUser(bytes32 userName);
    event ChangedAddress(bytes32 userName, address newAddress);
    event modifiedUserWot(bytes32 userName, bytes32 value);
    event modifiedPubKey(bytes32 userName);

    modifier onlyUser(bytes32 userName) {
        require(msg.sender == users[userName].ethereumAddress);
        _;
    }
    modifier onlyUserOrOwner(bytes32 userName) {
        require(
            (msg.sender == users[userName].ethereumAddress) ||
            (msg.sender == users[ownerName].ethereumAddress)
        );
        _;
    }

    function createUser(bytes32 userName, address userAddress) external payable
    existsUser(userName, true)
    existsAddress(userAddress, true)
    returns (bool) {
        /*
         * @dev TODO: User should only be able to register its own address
         *            (userAddress == msg.sender)
         *            or at least we should check address not matches any!
         */
        require(msg.value == UserFee);

        users[userName].ethereumAddress = userAddress;
        addr2user[userAddress] = userName;

        // Why not working?
        //setMultihash(users[userName].wot, bytes32(0x0));
        // Assigning:
        //  users[userName].wot.alg = 18; -> Works
        //  users[userName].wot.len = 32; -> Doesn't! wtf?!
        // initializate pubkey

        emit NewUser(userName, userAddress);
        return true;
    }

    function deleteUser(bytes32 userName) external
    onlyUser(userName)
    returns (bool) {
        /*
         * @dev TODO: More info on gap when deleted entry
         */
        address userAddress = users[userName].ethereumAddress;
        delete users[userName];
        delete addr2user[userAddress];
        emit DeletedUser(userName);
        return true;
    }

    function changeAddress(bytes32 userName, address newAddress) external
    onlyUser(userName)
    onlyOwner(true)
    existsAddress(newAddress, true)
    returns (bool){
        /*
         * @notice: Address of the owner user can only be changed
         *          through changeOwnership()
         */
        address userAddress = users[userName].ethereumAddress;
        users[userName].ethereumAddress = newAddress;
        addr2user[userAddress] = bytes32(0);
        emit ChangedAddress(userName, newAddress);
        return true;
    }

    function setUserWot(bytes32 userName, bytes32 value, bool flag_public) external
    onlyUser(userName)
    returns (bool) {
        /*
         * @notice: Set the CID (IPFS identifier) of the user's WoT
         *          Used for both local and global WoT
         */
        setMultihash(users[userName].wot, value);
        if (flag_public) {
            emit modifiedUserWot(userName, value);
        }
        return true;
    }

    function setPubKey_half(bytes32 userName, bytes32 value, bool whichHalf) external
    onlyUser(userName)
    returns (bool) {
        /*
         * @notice Set half of the user's public key.
         *         Separated as two halves so that they fit in bytes32:
         *         for efficiency.
         *          whichHalf == 0 -> set first half
         *          whichHalf == 1 -> set second half
         */
        if (whichHalf) {
            users[userName].pubkey.half1 = value;
        } else {
            users[userName].pubkey.half0 = value;
        }
        emit modifiedPubKey(userName);
        return true;
    }

    function queryAddress(bytes32 userName) external view
    returns (address) {
        /*
         * @return User's ethereum address
         *         If returned address is 0x0 -> user does not exist
         */
        return users[userName].ethereumAddress;
    }

    function queryPubKey_half(bytes32 userName, bool whichHalf) external view
    returns (bytes32) {
        /*
         * @return Half of the user's public key.
         *         Separated as two halves so that they fit in bytes32:
         *         for efficiency.
         *         If returned address is 0x0 -> user does not exist
         *          whichHalf == 0 -> query first half
         *          whichHalf == 1 -> query second half
         */
        return whichHalf? users[userName].pubkey.half1 : users[userName].pubkey.half0;
    }

    function queryUser(address userAddress) external view
    returns (bytes32) {
        /*
         * @return User name as bytes32.
         *         If returned bytes32 is 0x0 -> address does not exist
         */
        return addr2user[userAddress];
    }

    function queryUserWot(bytes32 userName) external view
    onlyUserOrOwner(userName)
    returns (bytes32) {
        /*
         * @return CID of user's WoT as bytes32.
         */
        return users[userName].wot.val;
    }
}
