pragma solidity ^0.5.0;

import "./Storage.sol";

contract Resources is Storage {
    /*
     * @title Title
     * @author Luis Colmenero Sendra
     * @notice Inherits from Storage.sol and will be inherited by PKI.sol
     *         Contains resource-specific functions.
     */

    event NewResource(bytes32 resourceHash, bytes32 resourceAuthor);
    event DeletedResource(bytes32 resourceHash);
    event modifiedResourceWot(bytes32 resourceHash, bytes32 newWot);
    event modifiedResourceKeys(bytes32 resourceHash, bytes32 newKeys);
    event modifiedResourceFile(bytes32 resourceHash, bytes32 newFile);

    modifier onlyAuthor(bytes32 resourceHash) {
        // resource author: name > address
        bytes32 resourceAuthor = resources[resourceHash].author;
        address resourceAuthorAddress = users[resourceAuthor].ethereumAddress;

        require(msg.sender == resourceAuthorAddress);
        _;
    }
    modifier onlyAuthorOrOwner(bytes32 resourceHash) {
        /*
         * modifyResourceKeys can be updated also from the author when chat
         * server active. That way session key is never at owner
         */
        bytes32 resourceAuthor = resources[resourceHash].author;
        address resourceAuthorAddress = users[resourceAuthor].ethereumAddress;
        require(
            (msg.sender == resourceAuthorAddress) ||
            (msg.sender == users[ownerName].ethereumAddress)
        );
        _;
    }

    function createResource(bytes32 resourceHash) external payable
    existsResource(resourceHash, true)
    existsUser(addr2user[msg.sender], false)
    returns (bool success) {
        /*
         * @dev TODO: set initial wot to hash of only resourceAuthor
         *      Left as future work because reproducing hash calculated by they
         *      js app in solidity is not trivial.
         *      So far initialized to empty (0x0).
         * @dev TODO: replace
         *      resources[resourceHash].wot = 0x0;
         *      with
         *      setMultihash(resources[resourceHash].keys, bytes32(0x0));
         *      Don't know why but it is not working.
         */
        require(msg.value == ResourceFee);

        bytes32 resourceAuthor = addr2user[msg.sender];
        resources[resourceHash].author = resourceAuthor;

        resources[resourceHash].wot = 0x0;

        emit NewResource(resourceHash, resourceAuthor);

        return true;
    }

    function deleteResource(bytes32 resourceHash) external
    onlyAuthor(resourceHash)
    returns (bool) {
        /*
         * @dev TODO: More info on gap when deleted entry
         */
        delete resources[resourceHash];
        emit DeletedResource(resourceHash);
        return true;
    }

    function queryResourceAuthor(bytes32 resourceHash) external view
    returns (bytes32) {
        /*
         * @return Author name as bytes32
         *         If returned name is 0 -> resource does not exist
         */
        return resources[resourceHash].author;
    }

    function queryResourceWot(bytes32 resourceHash) external view
    returns (bytes32) {
        /*
         * @return bytes32 hash of the global wot for a resource.
         *         This is the resource's attribute representing the hash
         *         (sha3) of all allowed users.
         */
        return resources[resourceHash].wot;
    }

    function queryResourceKeys(bytes32 resourceHash) external view
    returns (bytes32) {
        /*
         * @return Query CID of the file containing the keys associated to the
         *         resource. Allowed users can get session key from reading it.
         */
        return resources[resourceHash].keys.val;
    }

    function queryResourceFile(bytes32 resourceHash) external view
    returns (bytes32) {
        /*
         * @return CID for the file containing encrypted resource (with session
         *         key).
         */
        return resources[resourceHash].file.val;
    }

    function modifyResourceAuthor(bytes32 resourceHash, bytes32 newAuthor) external
    existsResource(resourceHash, false)
    onlyAuthor(resourceHash)
    // existsUser(newAuthor, false) --> Stack too dep
    returns(bool) {
        if (users[newAuthor].ethereumAddress != address(0x0)) { // existsUser
            resources[resourceHash].author = newAuthor;
            return true;
        } else {
            return false;
        }
    }

    function modifyResourceWot(bytes32 resourceHash, bytes32 newWot) external
    existsResource(resourceHash, false)
    onlyAuthorOrOwner(resourceHash)
    // onlyOwner(false)
    returns (bool) {
        /*
         * @notice Set the hash of the global wot for a resource.
         *         This is the resource's attribute representing the hash
         *         (sha3) of all allowed users.
         */
        resources[resourceHash].wot = newWot;
        emit modifiedResourceWot(resourceHash, newWot);
        return true;
    }

    function modifyResourceKeys(bytes32 resourceHash, bytes32 newKeys) external
    existsResource(resourceHash, false)
    onlyAuthorOrOwner(resourceHash)
    // onlyOwner(false)
    returns (bool) {
        /*
         * @notice Set CID of the file containing the keys associated to the
         *         resource. Allowed users can get session key from reading it.
         */
        setMultihash(resources[resourceHash].keys, newKeys);
        emit modifiedResourceKeys(resourceHash, newKeys);
        return true;
    }

    function modifyResourceFile(bytes32 resourceHash, bytes32 newFile) external
    existsResource(resourceHash, false)
    onlyAuthor(resourceHash)
    returns (bool) {
        /*
         * @notice Set CID for the file containing encrypted resource (with
         *         session key).
         */
        setMultihash(resources[resourceHash].file, newFile);
        emit modifiedResourceFile(resourceHash, newFile);
    }
}
