(TeX-add-style-hook
 "tfm"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "11pt" "twocolumn" "hidelinks")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8") ("hypcap" "all") ("eurosym" "official") ("ulem" "normalem") ("breakurl" "hyphenbreaks")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "fontenc"
    "inputenc"
    "lmodern"
    "xcolor"
    "hyperref"
    "hypcap"
    "graphicx"
    "subcaption"
    "multirow"
    "booktabs"
    "eurosym"
    "ulem"
    "url"
    "breakurl")
   (TeX-add-symbols
    '("commentswho" 2)
    '("doubtwho" 2)
    '("modifiedsout" 1)
    '("modified" 1)
    '("commented" 1)
    '("commentsdarg" 1)
    '("comments" 1))
   (LaTeX-add-labels
    "tab:relatedWork"
    "fig:overview"
    "fig:webapp"
    "tab:functions")
   (LaTeX-add-bibliographies
    "references")
   (LaTeX-add-xcolor-definecolors
    "dgreen"))
 :latex)

